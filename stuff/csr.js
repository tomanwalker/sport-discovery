
//## dependencies
var cp = require('child_process');
var fs = require('fs');

//## funcs
var print = function(...msg){
	return console.log(...msg);
};

//## flow
print('*** CSR utility ***');

var config = require('./domains.json');
print('>> Config loaded - url.length = %s', config.url.length);

for(var i=0; i<config.url.length; i++){
	
	var url = config.url[i];
	
	// generate CSR(s)
	// openssl req -out CSR.csr -new -newkey rsa:2048 -nodes -keyout privatekey.key \
	// -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=example.com"
	var csr_cmd = `openssl req -nodes -newkey rsa:2048 -out output/${url}.csr -keyout output/${url}.key \\`;
	csr_cmd += `-subj "/C=${config.csr.C}/ST=${config.csr.ST}/L=${config.csr.L}/O=${config.csr.O}/OU=${config.csr.OU}/CN=${url}"`;
	
	print(">> csr command = %s", csr_cmd);
	result = cp.execSync(csr_cmd);
	var csrOutput = result.toString();
	
	print(csrOutput);
	
}

print('*** Done ***');


