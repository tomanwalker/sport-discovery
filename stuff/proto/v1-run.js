
// ## dependencies
var common = require('./common');

// ## config
var FILE = 'all-rio-2016-athletes-excel.csv';
/*
var TARGET_MONTH = 12;
var TARGET_HEIGHT = 178;
var TARGET_SEX = 'male';
var RANGE_HEIGHT = 6;
var RANGE_MONTH = 1;
*/

/*
var TARGET_MONTH = 9;
var TARGET_HEIGHT = 174;
var TARGET_SEX = 'male';
var RANGE_HEIGHT = 2;
var RANGE_MONTH = 2;
*/

var TARGET_MONTH = 12;
var TARGET_HEIGHT = 166;
var TARGET_SEX = 'female';
var RANGE_HEIGHT = 2;
var RANGE_MONTH = 2;

var IGNORE = ['canoe', 'sailing', 'equestrian', 'shooting',
	'handball', 'wrestling', 'fencing', 'modern pentathlon',
	'golf', 'archery', 'rowing', 'rugby sevens', 'weightlifting', 'badminton'];
var LIKE = ['basketball', 'judo', 'taekwondo'];
var DISLIKE = ['boxing', 'aquatics', 'athletics', 'cycling', 'triathlon', 'volleyball', 'hockey'];
var TOP = 5;

// ## func


// ## flow

console.log('** start **');
console.log('Looking for - month = %s | height = %s (+-%s) | sex = %s', 
	TARGET_MONTH, TARGET_HEIGHT, RANGE_HEIGHT, TARGET_SEX);

var file = FILE;
var data = common.readFile(file);

/* {"name":"A Jesus Garcia","sport":"athletics","nationality":"ESP","sex":"male",
* "date_of_birth":"17/10/1969","age (at start of games)":"46,8","height (m)":"1,72","weight (kg)":"64",
* "BMI":"21,6","gold":"0","silver":"0","bronze":"0","":""}
*/
var filtered = data.filter( x => x.sex === TARGET_SEX );
console.log('main - filter - cnt = %s', filtered.length);

var rangeMonth = [];
for(var i=-RANGE_MONTH; i<=RANGE_MONTH; i++){
	rangeMonth.push(i);
}

rangeMonth = rangeMonth.map(function(x){
	x = x + TARGET_MONTH;
	if( x > 12 ){
		x = x - 12;
	}
	return x;
});

var mapped = filtered.map(function(x){
	x.month = Number(x['date_of_birth'].split('/')[1]);
	x.height = Number(x['height (m)'].replace(',', '.')) * 100;
	
	x.m = rangeMonth.includes(x.month);
	x.h = (x.height >= (TARGET_HEIGHT - RANGE_HEIGHT) && x.height <= (TARGET_HEIGHT + RANGE_HEIGHT));
	x.medals = Number(x.gold) * 100 + Number(x.silver) * 10 + Number(x.bronze);
	x.w = (x.medals > 0);
	
	return x;
});
console.log('main - mapped - sample = %j', mapped[0]);

var match = mapped.filter(function(x){
	return x.h && x.m;
}).sort(function(a,b){
	return b.medals - a.medals;
});
console.log('main - match - cnt = %s', match.length);

var counter = {};
var metrics = {};
match.forEach(function(x){
	var key = x.sport;
	if( typeof(counter[key]) === 'undefined' ){
		counter[key] = 0;
		metrics[key] = {h_sum: 0, m: {}};
	}
	counter[key] += 1;
	
	metrics[key].h_sum += x.height;
	
	if( typeof( metrics[key].m[x.month] ) === 'undefined' ){
		metrics[key].m[x.month] = 0;
	}
	metrics[key].m[x.month] += 1;
});
console.log('main - counter = %j', counter);

var list = Object.entries(counter).map(function(x){
	return {k: x[0], v: x[1]};
});
list = list.sort((a, b) => b.v - a.v);
console.log('----------------------------');
console.log('main - list = %j', list);
console.log('----------------------------');

console.log('BEFORE preferences applied: ');
console.log('TOP 3 = %s', list.slice(0,3).map(x => `${x.k} = ${x.v}`).join(' | '));
console.log('LAST 3 = %s', list.slice(-3).map(x => `${x.k} = ${x.v}`).join(' | '));

console.log('----------------------------');
console.log('Considering preferences...');

list = list.filter(function(x){
	if( IGNORE.includes(x.k) ){
		console.log('Ignore = %s', x.k);
		return false;
	}
	return true;
});

list = list.map(function(x){
	if( LIKE.includes(x.k) ){
		console.log('Like = %s', x.k);
		x.v = x.v * 2;
	}
	if( DISLIKE.includes(x.k) ){
		console.log('DisLike = %s', x.k);
		x.v = Math.round(x.v / 2);
	}
	
	return x;
});
list = list.sort((a, b) => b.v - a.v);

var best = list.slice(0, TOP);

console.log('ACCOUNT for preferences: ');
console.log('TOP %s (pref) = %s', TOP, best.map(x => `${x.k} = ${x.v}`).join(' | '));
console.log('LAST %s (pref) = %s', TOP, list.slice(-TOP).map(x => `${x.k} = ${x.v}`).join(' | '));
console.log('----------------------------');
console.log('main - list (preferences) = %j', list);

console.log('----------------------------');
LIKE.forEach(function(el){
	console.log('%s - sample (pref) = %j', el, match.find( x => x.sport === el ) );
});
console.log('###');
best.forEach(function(el){
	console.log('%s - sample (top) = %j', el.k, match.find( x => x.sport === el.k ) );
});

console.log('======FINAL======');
console.log('%s', list.map(x => x.k + ' = ' + x.v).join("\n"));





