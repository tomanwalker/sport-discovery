
// ## dependencies
var fs = require('fs');
var csv = require('csv-parse/lib/sync');

// ## export
var ns = {};
module.exports = ns;

// ## funcs
ns.readFile = function(filename){
	
	console.log('readFile - start - file = %s', filename);
	var content = fs.readFileSync(filename);
	var records = csv(content, {
		delimiter: ";",
		columns: true
	});
	
	console.log('readFile - loaded - sample = %j', records[0]);
	return records;
};


