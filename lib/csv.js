
// ## dependencies
var fs = require('fs');
var csv = require('csv-parse/lib/sync');
var fastcsv = require('fast-csv');

// ## export
var ns = {};
module.exports = ns;

// ## funcs
ns.readFile = function(filename, del){
	
	if( typeof(del) === 'undefined' ){
		del = ";";
	}
	
	console.log('readFile - start - file = %s', filename);
	var content = fs.readFileSync(filename);
	var records = csv(content, {
		delimiter: del,
		columns: true
	});
	
	console.log('readFile - loaded - sample = %j', records[0]);
	return records;
};

ns.writeFile = function(filename, data, dry){
	console.log('writeFile - before - name = %s | cnt = %s', filename, data.length);
	var mid = Math.round(data.length/2);
	console.log('writeFile - before - sample = %j', data[mid]);
	
	if( dry ){
		return true;
	}
	var stream = fs.createWriteStream(filename);
	return fastcsv.write(data, { headers: true, delimiter: ';' }).pipe(stream);
};






