
// ## dependencies
var fs = require('fs');

var logger = require('./logger');

// ## export
var ns = {};
module.exports = ns;

// ## config
var VIEW_EXT = 'html';

var log = logger.getLogger('view');

// ## func
ns.render = function(page, bag, template){
	
	log.debug('render - start - page = %s | template = %s | bag = %j', page, template, Object.keys(bag));
	
	/*
		{
		"sub": "123",
		"name": "Test",
		"picture": "https://server.com/photo.jpg",
		"email": "test@mail.com",
		"email_verified": true,
		"locale": "en",
		"provider": "google",
		"got": "2020-10-28T12:17:39.155Z",
		"allow": 101
		};
	*/
	// Return only subset of attributes to minimize exposure
	if( bag.user ){
		bag.user = {
			id: bag.user.id,
			name: bag.user.name,
			picture: bag.user.picture,
			locale: bag.user.locale,
			allow:  bag.user.allow
		};
		if( bag.user.allow >= 100 && bag.user.allow < 300 ){
			bag.user.rank = 'casual'
		}
		if( bag.user.allow >= 300 && bag.user.allow < 500 ){
			bag.user.rank = 'elite'
		}
		if( bag.user.allow >= 500 ){
			bag.user.rank = 'supreme'
		}
	}
	
	if( typeof(template) === 'undefined' ){
		template = 'template';
	}
	
	var mainContent = '%{view}';
	if( template ){
		mainContent = fs.readFileSync('views/' + template + '.' + VIEW_EXT, 'utf8');
	}
	
	var pageContent = (
		'<div class="row"><div class="col-lg-12 text-center"><br /><br />'
		+ '<h3>Something went wrong. Try again later</h3>'
		+ '</div></div>'
	);
	try{
		pageContent = fs.readFileSync('views/' + page + '.' + VIEW_EXT, 'utf8');
	}
	catch(err){
		log.error('render - content - err = %j', err);
		/*
		{"message":"ENOENT: no such file or directory, open 'views/articles/undefined.html'",
		*/
		var statusToSend = err.message.includes('ENOENT') ? 404 : 500;
		throw {statusCode: statusToSend, message: err.message};
	}
	var merged = mainContent.replace('%{view}', pageContent);
	
	var regex = /\%\{.*\}/g;
	var matches = merged.match(regex).map(x => x.substring(2, x.length-1) );
	log.debug('render - regex - matches = %j', matches);
	var debugObj = {};
	
	matches.forEach(function(x){
		
		var val = null;
		
		if( x.includes('.') ){
			var sp = x.split('.');
			sp.forEach(function(lvl){
				val = ( val || bag )[lvl];
			});
		}
		else {
			val = bag[x];
		}
		
		if( typeof(val) === 'undefined' ){
			val = null;
		}
		if( typeof(val) === 'object' && val !== null ){
			val = JSON.stringify(val);
		}
		
		debugObj[x] = val;
		merged = merged.replace('%{' + x + '}', val);
	});
	
	log.debug('render - replaced - props = %j ', Object.keys(debugObj) );
	return merged;
};


