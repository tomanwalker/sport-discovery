
// ## dependencies

// ## export
var ns = {};
module.exports = ns;

// ## config

// ## func
ns.getLogger = function(name){
	
	var print = function(level){
		return function(...msg){
			var ts = new Date();
			msg[0] = ts.toISOString() + ' - ' + name + ' [' + level + '] >> ' + msg[0];
			return console.log(...msg);
		};
	};
	
	return {
		debug: print('DEBUG'),
		info: print('INFO'),
		error: print('ERROR')
	};
};


