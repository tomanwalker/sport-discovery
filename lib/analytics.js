
// ## dependencies


// ## export
var ns = {};
module.exports = ns;

// ## config
var log = require('./logger').getLogger('lib/analytics');

// ## func
ns.pct = function(arr, q, already_sorted){
	
	var sorted = (already_sorted) ? arr : arr.sort();
	var len = sorted.length;
	var pos = (len - 1) * q;
	var base = Math.floor(pos);
	var rest = pos - base;
	
	if (sorted[base + 1] !== undefined) {
		return sorted[base] + rest * (sorted[base + 1] - sorted[base]);
	} else {
		return sorted[base];
	}
};

ns.pctRank = function(array, v) {
	if (typeof(v) !== 'number') {
		throw new TypeError('v must be a number');
	}
	var LESS = 0;
	var SAME = 0;
	var GREATER = 0;
	var N = array.length;
	
	for (var i = 0; i < N; i++) {
		if (array[i] < v) {
			LESS += 1;
		} else if (array[i] === v){
			SAME += 1;
		} else {
			GREATER += 1;
		}
	}
	
	var pct = (LESS + (0.5 * SAME)) / N;
	return pct;
};

ns.group = function(list, groupKeys, special){
	
	if( typeof(groupKeys) === 'undefined' ){
		groupKeys = ['sport'];
	}
	if( typeof(special) === 'undefined' ){
		special = {};
	}
	if( typeof(special.winnerAdjust) === 'undefined' ){
		special.winnerAdjust = true;
	}
	
	var groups = {};
	var groupStats = {};
	var calcTable = {};
	
	list.forEach(function(x){
		
		var key = groupKeys.map(function(k){
			return x[k];
		}).filter(function(v){
			return v !== '';
		}).join(':');
		
		if( !groups[key] ){
			groups[key] = [];
			groupStats[key] = {key: key, cnt: 0};
			calcTable[key] = {key: key, h_sum: 0, height: [], bmi:[] };
		}
		
		groups[key].push(x);
		groupStats[key].cnt += 1;
		
		var pushTimes = 1;
		
		if( special.winnerAdjust ){
			// winner influence population percentile
			if( x.medal_number > 0 ){
				pushTimes += 1;
			}
			if( x.medal_number >= 10 ){
				pushTimes += 2;
			}
			if( x.medal_number >= 100 ){
				pushTimes += 3;
			}
		}
		for(var z=0; z<pushTimes; z++){
			if( x.height && x.height > 10 ){
				calcTable[key].h_sum += x.height;
				calcTable[key].height.push(x.height);
			}
			
			if( x.bmi && x.bmi > 5 ){
				calcTable[key].bmi.push(x.bmi);
			}
		}
	});
	
	var foundKeys = Object.keys(calcTable);
	foundKeys.forEach(function(c){
		calcTable[c].height = calcTable[c].height.sort();
		calcTable[c].bmi = calcTable[c].bmi.sort();
	});
	
	return {
		groups: groups,
		groupStats: groupStats,
		calcTable: calcTable,
		groupKeys: foundKeys
	};
};

ns.round = function(num, dec){
	if( typeof(dec) === 'undefined' ){
		dec = 0;
	}
	var divider = Math.pow(10, dec);
	return Math.round( num * divider ) / divider;
};


