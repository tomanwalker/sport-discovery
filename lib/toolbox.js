
// ## dependencies
var validator = require('jsonschema').validate;
var request = require('request');

var logger = require('./logger');
var config = require('../config');

// ## export
var ns = {};
module.exports = ns;

// ## config
var log = logger.getLogger('toolbox');

// ## func
ns.config = config;
ns.getLogger = logger.getLogger;
ns.render = require('./view').render;
ns.dal = require('./dal');
ns.analytics = require('./analytics');
ns.csv = require('./csv');
ns.schemas = null; // on init

ns.validate = function(body, schema){
	
	var err = validator(body, schema);
	
	if( err && err.errors && err.errors.length > 0 ){
		var msg = err.errors.map( x => x.stack.replace(/instance/g, "input") ).join("<br />");
		var att = err.errors.map(function(x){
			return {
				"prop": x.property,
				"rule_name": x.name,
				"rule_val": x.argument
			};
		});
		return {
			message: msg,
			details: att
		};
	}
	return null;
};

ns.auth = {};
ns.auth.roles = [
	{num: 555, label: 'supreme'},
	{num: 333, label: 'elite'},
	{num: 111, label: 'casual'}
];
ns.auth.check_auth = function(redirect){
	return function(req, res, next){
		
		var check = req.isAuthenticated && req.isAuthenticated();
		var user = (req.user || {});
		
		log.debug('auth.check_auth - start - check = %s | got = %s', check, user.got);
		if( check ){
			return next();
		}
		
		log.debug('auth.check_auth - nope - url = %s', req.originalUrl);
		req.session.url = req.originalUrl;
		if( redirect ){
			return res.redirect(redirect + '?r=' + req.path.slice(1));
		}
		return res.status(401).json({message: 'Not authorized'});
	};
};

ns.auth.verify_user = async function( username, userObj ){
	log.debug('auth.verify_user - start - username = %s', username);
	
	var result = null;
	try{
		result = await ns.dal.users.search({username: username});
	}
	catch(err){
		if( err.statusCode !== 404 ){
			throw err;
		}
		
	}
	if( result === null ){
		//create new user
		userObj.username = username;
		userObj.allow = 101;
		userObj.provider = 'google';
		
		ns.hook.post('newuser', username);
		var createResult = await ns.dal.users.create(userObj);
		result = userObj;
		result._id = createResult.id;
	}
	
	return result;
};

ns.auth.init = function(router){
	// ## dependencies
	var passport = require('passport');
	var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
	var session = require('express-session');
	var sessionFileStore = require('session-file-store')(session);
	
	// ## config
	var scope = [
		'https://www.googleapis.com/auth/userinfo.email', 
		'https://www.googleapis.com/auth/userinfo.profile'
	];
	var passOpts = {
		clientID: config.GOOGLE_ID,
		clientSecret: config.GOOGLE_SECRET,
		callbackURL: config.APP_URL + '/callback'
	};
	var sessionOpts = {
		secret: config.SESSION_SECRET,
		resave: false,
		saveUninitialized: false,
		cookie: {
			maxAge: (7 * 24 * 60 * 60 * 1000) // 7 days
		},
		store: new sessionFileStore({fileExtension: '.txt'})
	};
	
	// 20201103 not working properly
	/*
	if( ns.config.CLOUD_RUN && ns.config.SECURE_REDIRECT === 'true' ){
		sessionOpts.cookie.sameSite = true;
		sessionOpts.cookie.secure = true;
	}
	else {
		sessionOpts.store = new sessionFileStore({fileExtension: '.txt'});
	}
	*/
	
	passport.use(new GoogleStrategy( passOpts,
		function(accessToken, refreshToken, profile, done){
			
			log.debug('auth.strategy - user - %s | a_t = %s | r_t = %s | profile = %j', 
				profile.displayName, accessToken.substring(0,4), 
				(refreshToken|| "none").substring(0,4),
				profile
			);
			
			// verify user
			var email = 'none';
			if( profile && profile.emails && profile.emails.length
				&& profile.emails[0] && profile.emails[0].verified ){
				
				email = profile.emails[0].value;
			}
			
			var userObj = profile._json;
			
			ns.auth.verify_user(email, userObj).then(function(result){
				log.debug('auth.strategy - verify_user - result = ok | email = %s', email);
				userObj.allow = result.allow;
				userObj.id = result._id;
			}).catch(function(err){
				log.debug('auth.strategy - verify_user - result = fail | email = %s | err = %j', email, err);
				//return done(null, false, { message: 'User not found' });
			}).finally(function(){
				
				userObj.got = (new Date()).toISOString();
				
				if( typeof(userObj.provider) === 'undefined' ){
					userObj.provider = 'google';
				}
				if( typeof(userObj.allow) === 'undefined' ){
					userObj.allow = 101;
				}
				
				return done(null, userObj);
			});
			
		}
	));
	
	router.use( session(sessionOpts) );
	router.use( passport.initialize() );
	router.use( passport.session() );
	
	passport.serializeUser(function(user, done) {
		var id = user.email;
		//log.debug('auth.serializeUser - user - %s', id);
		done(null, user);
	});
	passport.deserializeUser(async function(user, done) {
		
		user.fetch = false;
		var shouldReload = ns.dal.reload[user.id];
		
		// fetch profile once in 20 minutes
		var FETCH_LIMIT = 1 * 60 * 1000;
		
		var current = new Date();
		var gotTime = new Date(user.got);
		
		var timeDiff = (current - gotTime);
		log.debug('auth.deserializeUser - id = %s | forceReload = %s | time - %s / %s / %s', 
			user.id, shouldReload, (timeDiff > FETCH_LIMIT), timeDiff, FETCH_LIMIT);
		
		if( shouldReload || timeDiff > FETCH_LIMIT ){
			
			try{
				var record = null;
				log.debug('auth.deserializeUser - before fetch - record.id - %j', user.id);
				if( user.id ){
					record = await ns.dal.users.get(user.id);
				}
				else {
					record = await ns.dal.users.search({username: user.email});
					user.id = record._id;
				}
				
				log.debug('auth.deserializeUser - fetched - record - %j', record);
				user.got = current.toISOString();
				user.allow = record.allow;
				user.fetch = true;
				
				if( shouldReload ){
					delete ns.dal.cache.reload[user.id];
				}
				
			}
			catch(err){
			}
		}
		
		if( typeof(user.allow) === 'undefined' ){
			user.allow = 101;
		}
		
		done(null, user);
	});
	
	//   Use passport.authenticate() as route middleware to authenticate the
	//   request.  The first step in Google authentication will involve
	//   redirecting the user to google.com.  After authorization, Google
	//   will redirect the user back to this application at /auth/google/callback
	router.get('/auth/login',
		passport.authenticate('google', { scope: scope })
	);
	//   Use passport.authenticate() as route middleware to authenticate the
	//   request.  If authentication fails, the user will be redirected back to the
	//   login page.  Otherwise, the primary route function function will be called,
	//   which, in this example, will redirect the user to the home page.
	router.get('/callback', 
		passport.authenticate('google', { failureRedirect: '/login' }),
		function(req, res) {
			// https://oauth2.example.com/auth?error=access_denied
			log.debug('router.callback - good - user = %s', req.user);
			req.session.token = req.user.token;
			
			log.debug('router.callback - redirect - url = %s', req.session.url);
			if( !req.session.url ){
				req.session.url = '/';
			}
			
			req.session.debug = 1;
			req.session.save(function(err){
				log.debug('router.callback - session.save - done...');
				return res.redirect( req.session.url );
			});
		}
	);
	
	router.get('/auth/logout', function(req, res, next){
		
		var url = req.session.url || '/';
		var targetUser = res.locals.username;
		log.debug('router.logout - start - user = %s', targetUser);
		
		req.logout();
		req.session.destroy(function(){
			log.debug('router.logout - done - user = %s', targetUser);
			return res.redirect(url);
		});
	});
	
};

ns.hook = {};
ns.hook.post = function(category, msg, label){
	
	var date = new Date();
	var dateStr = date.toISOString();
	log.debug('hook.post - start - d = %s | cat = %s | msg = %s', dateStr, category, msg);
	
	if( !config.CLOUD_RUN || !config.HOOK_EVENT ){
		return false;
	}
	
	var RATE_LIMIT = Math.round((60 * 1000) / 5); // 5 per minute
	var lastTime = ns.hook.last || new Date('2020-01-01T01:00:00.000Z');
	ns.hook.last = date;
	
	if( (date - lastTime) < RATE_LIMIT ){
		return false;
	}
	
	var content = "--------\n";
	content += dateStr + " - " + category + "\n";
	content += msg;
	
	var reqBody = {
		"username": "event-bot",
		"content": content
	};
	
	var reqOpts = {
		method: 'POST',
		timeout: 30000,
		url: config.HOOK_EVENT,
		body: reqBody,
		json: true
	};
	
	request(reqOpts, function(error, response, body){
		if( error ){
			log.error('hook.post - failed - err = %j', error);
		}
		if( response.statusCode.toString()[0] !== '2' ){
			log.error('hook.post - error - body = %j', body);
		}
		
		log.debug('hook.post - done - d = %s', dateStr);
	});
	
	return true;
};

ns.params = {};
ns.params.init = function(){
	log.debug('settings.init - start...');
	/*
	var yearMin = 3000;
	var yearMax = 1000;
	
	if( ns.config.CLOUD_RUN ){
		ns.dal.init();
	}
	var data = ns.dal.olympics.search();
	
	data.rows.forEach(function(x){
		yearMin = Math.min(x.year, yearMin);
		yearMax = Math.max(x.year, yearMax);
	});
	
	ns.params.YEAR_MIN = yearMin;
	ns.params.YEAR_MAX = yearMax;
	*/
	
	ns.params.YEAR_STEP = 10;
	ns.params.YEAR_MIN = 1980;
	ns.params.YEAR_MAX = 2018;
	ns.params.YEAR_ANON = ns.params.YEAR_MIN + ns.params.YEAR_STEP;
	ns.params.YEAR_CASUAL = ns.params.YEAR_ANON + ns.params.YEAR_STEP;
	
	ns.params.RESULT_LEN_ANON = 3;
	ns.params.RESULT_LEN_CASUAL = 4;
	ns.params.RESULT_LEN_ELITE = 5;
	
	log.debug('settings.init - done - params = %j', ns.params);
	ns.schemas = require('./schemas');
	
	return true;
};

ns.ping = async function(){
	log.debug('ping - start...');
	var result = await ns.dal.users.search({email: 'a'});
	log.debug('db - ping - done - result = %j', result);
};


