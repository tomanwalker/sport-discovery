
// ## dependencies
var cryptoLib = require("crypto");
var cloudantLib = require('@cloudant/cloudant');

var logger = require('./logger');
var csv = require('./csv');
var config = require('../config');

var dalFile = require('./dal.file.js');
var dalDb = require('./dal.db.js');

// ## export
var ns = {};
module.exports = ns;

// ## config
var CACHE_TIME = (1 * 60 * 60 * 1000); // 1 hour
var YEAR_LIMIT = 50;

ns.meta = {};
ns.olympics = {};
ns.nba = dalFile.nba;

ns.cache = {};
ns.reload = {};

ns.users = dalDb.users;
ns.feedback = dalDb.feedback;
ns.payments = dalDb.payments;
ns.reports = dalFile.reports;

var log = logger.getLogger('dal');

setInterval(function(){
	var current = new Date();
	Object.keys(ns.reload).forEach(function(k){
		var diff = current - ns.reload[k];
		if( diff > CACHE_TIME ){
			delete ns.reload[k];
		}
	});
});

// ## func
ns.getTimeId = function(){
	var date = new Date();
	var addLeading = function(val, len){
		if( typeof(len) === 'undefined' ){
			len = 2;
		}
		return ("0".repeat(len) + val).slice(-len);
	};
	
	var str = cryptoLib.randomBytes(8).toString('hex');
	return [date.getFullYear(), addLeading(date.getMonth()+1), addLeading(date.getDate()), str].join('-');
};

ns.olympics.search = function(query, special){
	log.debug('olympics.search - start - query = %j | special = %j', query, special);
	var current = new Date();
	var rawData = null;
	var seasonMap = null;
	
	if( typeof(query) === 'undefined' ){
		query = {};
	}
	if( typeof(special) === 'undefined' ){
		special = {};
	}
	
	if( ns.cache.olympics && ( current - ns.cache.olympics.ts ) < CACHE_TIME ){
		log.debug('olympics.search - cached - ts = %s', ns.cache.olympics.ts.toISOString());
		rawData =  ns.cache.olympics.data.map(a => Object.assign({}, a));
	}
	else {
		log.debug('olympics.search - loading athletes...');
		rawData = csv.readFile('data/olympics-modern.csv');
		
		ns.cache.olympics = {
			ts: current,
			data: rawData
		};
	}
	
	if( ns.cache.seasons && ( current - ns.cache.seasons.ts ) < CACHE_TIME ){
		seasonMap = ns.cache.seasons.data;
	}
	else {
		log.debug('olympics.search - loading seasons...');
		
		var seasonList = csv.readFile('data/sport-list.csv');
		seasonMap = {};
		seasonList.forEach(function(x){
			seasonMap[x.sport] = x.season;
		});
		
		ns.cache.seasons = {
			ts: current,
			data: seasonMap
		};
	}
	
	var mapped = rawData.map(function(x){
		// mapping
		x.height = Number(isNaN(x.height) ? 0 : x.height);
		x.weight = Number(isNaN(x.weight) ? 0 : x.weight);
		x.bmi = Number(isNaN(x.bmi) ? 0 : x.bmi);
		x.year = Number(x.year);
		
		x.medal_number = Number(x.medal_number);
		x.season = seasonMap[x.sport];
		
		return x;
	});
	
	log.debug('olympics.search - mapped sample - %j', mapped[0]);
	
	var eqAtt = ['sex', 'season'];
	var data = mapped.filter(function(x){
		
		if( query.year_start && x.year < query.year_start ){
			return false;
		}
		if( query.year_end && x.year > query.year_end ){
			return false;
		}
		
		for(var q in query){
			if( eqAtt.includes(q) && x[q] && query[q] !== x[q] ){
				return false;
			}
		}
		
		return true;
	});
	
	// dejoin after filter
	data = data.map(function(x){
		delete x.season;
		return x;
	});
	
	var obj = {
		total: data.length
	};
	
	if( special.quality === false ){
		obj.cnt = data.length;
		obj.rows = data;
		return obj;
	}
	
	// data quality
	var current_year = current.getFullYear();
	var year_check = current_year - YEAR_LIMIT;
	
	var oldAlert = [];
	var good = data.filter(function(x){
		// filter out missing
		if( x.height === 0 ){
			return false;
		}
		
		// too old
		if( x.year < year_check ){
			oldAlert.push(x);
			return false;
		}
		
		return true;
	});
	
	if( oldAlert.length > 0 ){
		log.error('olympics.search - too old - samples = %j', oldAlert.slice(0,5));
	}
	
	var memoryUsed = Math.round(process.memoryUsage().heapUsed / 1024 / 1024);
	log.debug('olympics.search - memory - used = %s mb', memoryUsed);
	
	obj.cnt = good.length;
	obj.rows = good;
	return obj;
};

ns.meta.get = function(id){
	if( typeof(id) === 'undefined' ){
		id = 'meta';
	}
	
	return require('../data/' + id + '.json');
};


