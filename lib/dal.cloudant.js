
/// ### Archive
ns.init = async function(){
	if( ns.cloudant ){
		return ns.cloudant;
	}
	
	var retryPlugin = { retry: { retryStatusCodes: [429] }};
	var iamPlugin = { iamauth: { iamApiKey: config.DB_KEY }};
	
	var cloudantOpts = {
		url: 'https://' + config.DB_HOST,
		plugins:[
			iamPlugin,
			retryPlugin
		]
	};
	
	ns.cloudant = new cloudantLib(cloudantOpts);
	var tableList = await ns.cloudant.db.list();
	log.debug('init - db check - list = %j', tableList);
	
	var indexConfig = [{
		db: config.DB_TABLE_USER,
		index: { name: 'username', type:'json', index:{fields:['username'] }}
	},{
		db: config.DB_TABLE_FEED,
		index: { name: 'review', type:'json', index:{fields:['review', 'timeCreate'] }}
	},{
		db: config.DB_TABLE_PAYMENT,
		index: { name: 'payment', type:'json', index:{fields:['kind', 'timeCreate', 'userid', 'username', 'category'] }}
	}];
	
	await indexConfig.forEach(async function(x){
		if( !tableList.includes(x.db) ){
			log.debug('init - create table - name = %s', x.db);
			await ns.cloudant.db.create( x.db );
		}
		
		var targetTable = ns.cloudant.use( x.db );
		var indexResult = await targetTable.index();
		log.debug('init - list index - db = %s | r = %j', x.db, indexResult);
		
		if( indexResult.indexes && !indexResult.indexes.find( z => z.name === x.index.name ) ){
			await targetTable.index(x.index);
		}
	});
	
	ns.cloudant.ready = true;
	return ns.cloudant;
};
ns.users.search = async function(query){
	if( !ns.cloudant ){
		await ns.init();
	}
	
	var userTableName = config.DB_TABLE_USER;
	var userIndexName = [userTableName].concat(Object.keys(query)).join('-');
	
	// check index
	log.debug('user.search - start - table = %s | index = %s | q = %j', userTableName, userIndexName, query);
	var indexTable = ns.cloudant.use( userIndexName );
	var userTable = ns.cloudant.use( userTableName );
	var indexKey = Object.values(query).join('-');
	
	try{
		var check = await indexTable.get( Object.values(query).join('-') );
		return await userTable.get(check.val);
	}
	catch(err){
		log.debug('user.search - index.err - status = %s | error = %s', err.statusCode, err.message);
		if( err.statusCode !== 404 ){
			throw err;
		}
		
		if( err.message.indexOf('Database') > -1 && err.message.indexOf('exist') > -1 ){
			await ns.cloudant.db.create( userIndexName );
			var indexTable = ns.cloudant.use( userIndexName );
		}
		
		// find record
		var queryResult = await userTable.find({ selector: query });
		log.debug('user.search - found - queryResult = %j', queryResult);
		if( queryResult.docs.length === 0 ){
			throw {statusCode: 404, message: 'not_found' }
		}
		
		// create Index
		var record = queryResult.docs[0];
		await indexTable.insert({ _id: indexKey, val: record._id });
		return record;
	}
};

ns.users.create = async function(obj){
	if( !ns.cloudant ){
		await ns.init();
	}
	
	var userTableName = config.DB_TABLE_USER;
	log.debug('user.create - start - table = %s | obj = %j', userTableName, obj);
	var userTable = ns.cloudant.use( userTableName );
	return userTable.insert(obj);
};
ns.users.get = async function(id){
	if( !ns.cloudant ){
		await ns.init();
	}
	
	var userTableName = config.DB_TABLE_USER;
	log.debug('user.get - start - table = %s | id = %s', userTableName, id);
	var userTable = ns.cloudant.use( userTableName );
	return userTable.get(id);
};
ns.users.update = ns.users.create;

ns.feedback.getLatest = async function(){
	
	log.debug('feedback.search - start...');
	var current = new Date();
	
	if( ns.cache.feed && (current - ns.cache.feed.ts < CACHE_TIME ) ){
		log.debug('feedback.search - cached - current = %s | ts = %s', 
			current.toISOString(), ns.cache.feed.ts.toISOString() );
		return ns.cache.feed.data;
	}
	
	if( !ns.cloudant ){
		await ns.init();
	}
	var feedTableName = config.DB_TABLE_FEED;
	var feedTable = ns.cloudant.use( feedTableName );
	var result = await feedTable.find({ selector: {review: 1}, sort: [{"timeCreate": "desc"}], limit: 10 });
	
	ns.cache.feed = {
		ts: current,
		data: result.docs
	};
	
	return result.docs;
};

ns.feedback.create = async function(input){
	log.debug('feedback.create - start - input = %j', input);
	if( !ns.cloudant ){
		await ns.init();
	}
	
	var feedTableName = config.DB_TABLE_FEED;
	var feedTable = ns.cloudant.use( feedTableName );
	return feedTable.insert(input);
};

ns.payments.create = async function(input){
	log.debug('payments.create - start - input = %j', input);
	if( !ns.cloudant ){
		await ns.init();
	}
	
	var payTableName = config.DB_TABLE_PAYMENT;
	var payTable = ns.cloudant.use( payTableName );
	return payTable.insert(input);
};



