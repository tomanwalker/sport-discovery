
// ## dependencies
var mongo = require('mongodb');
var crypto = require("crypto");

var config = require('../config');

// ## export
var ns = {};
module.exports = ns;

// ## config
var MongoClient = mongo.MongoClient;
var creds = `${config.DB_USER}:${config.DB_PASS}`;
var url = config.DB_URL || `mongodb://${creds}@${config.DB_HOST}:${config.DB_PORT}/`;
var client = null;
var db = null;

ns.users = {};
ns.feedback = {};
ns.payments = {};

// ## fund
ns.getRandomString = function(len){
	if( isNaN(len) && len < 1 ){
		len = 10;
	}
	return crypto.randomBytes(len).toString('hex');
};
ns.init = async function(){
	if( client === null ){
		console.log('dal.db.init - client is null - db init...');
		client = new MongoClient(url);
		await client.connect();
		db = client.db(config.DB_NAME);
		console.log('mongo.table - client.null - db done...');
	}
};

ns.insert = async function(tableName, doc){
	if( db === null ){
		await ns.init();
	}
	var table = db.collection(tableName);
	
	if( !doc._id ){
		doc._id = ns.getRandomString(14);
	}
	var result = await table.insertOne(doc);
	console.log('mongo.put - end - result = %j', result);
	return doc;
};

ns.users.search = async function(query){
	
	if( db === null ){
		await ns.init();
	}
	
	var table = db.collection(config.DB_TABLE_USER);
	var result = await table.findOne(query);
	return result;
};
ns.users.create = async function(obj){
	return await ns.insert(config.DB_TABLE_USER, obj);
};
ns.users.get = function(id){
	return ns.users.search({_id: id});
};
ns.users.update = ns.users.create;

ns.feedback.getLatest = async function(){
	return {};
};

ns.feedback.create = async function(input){
	return await ns.insert(config.DB_TABLE_FEED, input);
};

ns.payments.create = async function(input){
	return await ns.insert(config.DB_TABLE_PAYMENT, input);
};


