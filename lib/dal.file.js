
// ## dependencies
var logger = require('./logger');
var csv = require('./csv');
var config = require('../config');
var fs = require('fs');

// ## export
var ns = {};
module.exports = ns;

// ## config
var log = logger.getLogger('dal.file');

ns.nba = {};
ns.reports = {};

var CACHE_TIME = (1 * 60 * 60 * 1000); // 1 hour
ns.cache = {};

// ## func
ns.nba.search = function(query, special){
	
	log.debug('nba.search - start - query = %j | special = %j', query, special);
	var current = new Date();
	var rawData = null;
	
	if( typeof(query) === 'undefined' ){
		query = {};
	}
	if( typeof(special) === 'undefined' ){
		special = {};
	}
	
	if( ns.cache.nba && ( current - ns.cache.nba.ts ) < CACHE_TIME ){
		log.debug('nba.search - cached - ts = %s', ns.cache.nba.ts.toISOString());
		rawData =  ns.cache.nba.data.map(a => Object.assign({}, a));
	}
	else {
		log.debug('nba.search - loading athletes...');
		rawData = csv.readFile('data/nba.csv');
		
		ns.cache.nba = {
			ts: current,
			data: rawData
		};
	}
	
	var mapped = rawData.map(function(x){
		
		x.height = Number(x.height);
		x.weight = Number(x.weight);
		x.bmi = Number(x.bmi);
		x.year_start = Number(x.year_start);
		x.year_end = Number(x.year_end);
		x.medal_number = Number(x.medal_number);
		
		x.lg = 'nba';
		x.season = 'summer';
		return x;
	});
	
	var eqAtt = ['sex', 'season'];
	var data = mapped.filter(function(x){
		
		if( query.year_start && x.year_start < query.year_start ){
			return false;
		}
		if( query.year_end && x.year_end > query.year_end ){
			return false;
		}
		
		for(var q in query){
			if( eqAtt.includes(q) && x[q] && query[q] !== x[q] ){
				return false;
			}
		}
		
		return true;
	});
	
	log.debug('nba.search - done - total = %s | cnt = %s', mapped.length, data.length);
	return {
		total: mapped.length,
		cnt: data.length,
		rows: data
	};
};

ns.reports.bmi = function(){
	
	var content = fs.readFileSync('./data/reports/bmi.json');
	return JSON.parse(content);
};


