
// ## dep

var toolbox = require('./toolbox');

// ## config

// ## export
var ns = {};
module.exports = ns;

// ## fund
ns.schema = {};

ns.schema.draft = {
	required: ['gender', 'height'],
	additionalProperties: false, 
	properties: {
		gender: {
			type: 'string',
			'enum': ['male', 'female']
		},
		height: {
			type: 'integer',
			minimum: 140,
			maximum: 230
		},
		weight: {
			type: 'integer',
			minimum: 35,
			maximum: 210
		},
		checkSummer: {
			type: 'boolean'
		},
		checkWinter: {
			type: 'boolean'
		},
		yearRangeStart: {
			type: 'integer',
			minimum: toolbox.params.YEAR_MIN,
			maximum: (toolbox.params.YEAR_MAX - 8)
		},
		yearRangeEnd: {
			type: 'integer',
			minimum: (toolbox.params.YEAR_MIN + 8),
			maximum: toolbox.params.YEAR_MAX
		},
		units: {
			type: 'string',
			'enum': ['metric', 'imperical']
		},
		sets: {
			type: 'array',
			minItems: 1,
			maxItems: 2,
			items: {
				"type": 'string',
				'enum': ['olympics', 'nba'] // TODO replace with Meta.where.ready = 2
			}
		},
		checkDivision: {
			type: 'boolean'
		}
	}
};


