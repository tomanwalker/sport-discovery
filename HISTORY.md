

## [1.1.0] - 2020-10-15
### Added
- Login page
- data stats page (height & counts)
- data quality page (Admin only)
- Contribution page (chat event)
### Changed
- Draft Submit - server input validation

## [1.0.0] - 2020-10-11
### Added
- Deduplicate data
- GIT
- finder-v2
- start webpage ( Draft page )

- favicon.ico
- Nav coloring
- Input form  ( Draft page )
- Submit action ( Draft page )

- Draft server processing (POST /api/draft)
- Render Draft results
- icons for Sport table


