FROM node:lts-slim

ENV PORT 80
WORKDIR /app
EXPOSE ${PORT}

COPY . .
RUN npm install

CMD ["npm", "start"]

# docker build -t tomanwalker/sportdiv:latest -t tomanwalker/sportdiv:$(date +%Y%m%d) .
# docker image push tomanwalker/sportdiv:$(date +%Y%m%d)
# docker image push tomanwalker/sportdiv:latest
