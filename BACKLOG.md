
https://keepachangelog.com/en/1.0.0/
--------

=== TODO ===

- Data - Manually basketball separate PG (like - sub=pg/guard, 1.02% populated, need about 2%)
- Front - better landing
https://startbootstrap.com/themes/grayscale/
https://startbootstrap.github.io/startbootstrap-grayscale/

- Lab
>> body type calculator (popular on Google search)
>> Weight journal - rate of change, target weight Time stimate
>> Data/reasearch - weight loss / gain - success rate / useful methods

- Draft features:
>> Age group - little kid (Parents heights & dob)
>> Neck/wrist/Leg for Advanced options - need Antropology data first / can start easy - weight

- Common - can Render Protect (User/Login buttons, Rank/Boost button) on the server?
- [20201107] - Data - run Reports after a while (fix more Positions)
- Data - Add datasets (LeastPresent (Golf) / Olympics 2018 / FIFA / NHL / Some popular / LeastWomen (+Sport Presense Report) / MMA / BodyBuilding)
>> when list will get short - NFL / WNBA / MBL / FIBA / LPGA / Boxing or K1 / CrossFit

- Data - quality check with Olympedia (dob)
>> manually done Jan 62,63,64,65
>> need scripted

- Draft page / Stats - Cache results for faster response times
- Draft page - actual Bar chart for Height chart
- Draft page - persist results in Browser session to keep it after changing page
- Draft page - spinner GIF

- Draft page - call to action / next steps review
- About page - attributions / disclaimer
- Common - work on logo "Sport Division"
- Common - picture for LoggedIn user

- Feedback page - rating Slider / satisfaction survey / checkbox for permission to respond / add Feed on the right
- Quality page - quality index Widget
- Data - world population metrics
- api/draft - Save draft results for Stats page (eg. Last week - avg drafter is 176 cm)

- [after few users] Rank - handle Expiery
- Draft - check Tables size for Small screens
- checkbox to Loose requirements for more Random results????
- Data - think about checkbox team vs Single

- Draft page - Roster only (similar athletes only) - Contributor only
- Fit - printable Athlete card based on recorded results (SPECIAL / SAFES)
- Configure email - support@sportdiv.com
- Tools - BMI calc / BodyFat calc / Pulse stages

- Draft page - Medal Pictures instead of text (CSS is good as well)
- Draft page - Icons for Athlete country
- [After few users] Email marketing - new features (update About privacy)
- Draft page - default value (units, gender) based on Users popular choices (DB check)

- Draft page - Google for local clubs
- Articles
>> Positions - Weight classes overview (boxing, judo or takewondo)
>> Positions - why do weightlifing uses weight classes (a case for power-to-weight ratio)
>> Kids - "Month of birth influences results in sport - no horoscope involved"
>> Draft - "height in sports - just perfect"
>> Positions - Should running use Weight classes (a case for speed-to-weight ratio)

- Common - Imrerical system option to the Top-Right (like language) and use everywhere (eg Stats)
- Draft page - equipment/field difficulty (affordability)????
- Draft page - conditioning advice (how to prepare for the given sport)
- Same but different - judo=bjj / shooting=airsoft / snowboard=skateboard

- Admin page - logins, contributions, drafts
- Common - language support (RU / GE / ES / FI / FR)


=== DONE ===

## [Unreleased]
- Deploy & Put onto maintenance

## [1.5.3] - 2021-11-19
### Added
- Draft features:
>> Position play
>> On Roster sorting - group Participations into Single Athlete
>> Weight Filter

## [1.5.2] - 2021-04-28
### Added
- Articles
>> Datasets - NBA teams height diversity
### Changed
- Sitemap - added articles


## [1.5.1] - 2020-12-17
### Added
- Articles
>> Draft - "height in sports - outliers"
- Data - Double check Sub for "judo-open" combination
- Draft page - integrate datasets (NBA)
- Draft page - random commentator phrases
### Changed
- Sitemap - added "feedback" page
### Security
- Security - log error IPs


## [1.5.0] - 2020-11-11
### Added
- Report - month of birth stats
- Report - position stats
### Changed
- Data - populate 3716 subs based on Summer-Ol-76-08 (up to 6.76% sub-presense)
- Data - split Athletics into Subs (disciplines)
- Data - Position play check & test ( 72.13% -> good, may adjut AboveBelow step to down, to avoid too many overlaps)
- Common - Browser history ( add page name to the Title )
- Draft page - logged in user keep last value in Session
- Quality - new widget - missing Sub
### Fixed
- Algo - male 220 yields Short gymnastics - check sorting
- Algo - what happens on close 2,3 close matchups (female / 166cm) how sorting is done there


## [1.4.1] - 2020-11-05
### Added
- Stats page - Most females (percentage)
### Changed
- Front page - adjust Carousel Height for small screens
- Draft page - change PCT into Match index
- Stats page - integrate datasets (e.g NBA)

## [1.4.0] - 2020-11-03
### Added
- Home page - Jumbotron
- Data - NBA dataset v3 (1980-2018)
- About page - privacy
- Draft page - Imrerical system option
- Rank - donate button (hidden custom attr UserId)
- Common - add Google analytics
### Changed
- Draft page - Input to control Year range (1980-2016)
- Draft page - preview filters
- Stats - message about advanced tier getting more charts
- Feedback - checkbox for a permission to contact back
- Draft page - Results length - Anon only get 3 result / Casual 4 / Elite gets 5
- Review findings - ie header font, feedback alignment grid
### Fixed
- SSL setup
- dal.olympics - season query option


## [1.3.0] - 2020-10-27
### Added
- Data - NBA dataset v1
- Data - Script for bmi chart
- Data - BMI / Height distributions per gender (under, middle, over)
- Contribution page
- Stats page - Most/Least accessible (Height Low-High range sort)
### Changes
- Stats page - count as set cell, not a overview cell
- Register Domain name
###Fixed
- added missing image - Rowing
- api/draft - check to return only needed data attributes


## [1.2.0] - 2020-10-21
### Added
- Feedback page / form
- Template - add small Copyright footer
- Draft page - Season filter (logged in users)
- Cookies notification & consent
- Chat events (login, feedback, loggedin draft) / alerts (error)
### Changed
- api/draft - height AVG and MID difference to influence profile.pctRank
- Draft page - prevent submit if Missing data




