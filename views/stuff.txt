
// ### paypal.me

<a href="https://paypal.me/alexshkunov/5.75USD" target="_blank" class="btn btn-primary" >Paypal</a>

// ### donate button (real)
	<!-- LIVE -->
	<form action="https://www.paypal.com/donate" method="post" target="_top">
		<input type="hidden" name="hosted_button_id" value="JTCDJZW2LSRZU" />
		<input type="hidden" name="custom" value="" id="formCustomField" />
		<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
		<!-- <img alt="" border="0" src="https://www.paypal.com/en_FI/i/scr/pixel.gif" width="1" height="1" /> -->
	</form>

/// ### donate button (sandbox)
	<!-- SANDBOX -->
	<form action="https://www.sandbox.paypal.com/donate" method="post" target="_top">
		<input type="hidden" name="hosted_button_id" value="MUZ8YSS3ZZPSJ" />
		<input type="hidden" name="custom" value="" id="formCustomField" />
		<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
		<!-- <img alt="" border="0" src="https://www.sandbox.paypal.com/en_FI/i/scr/pixel.gif" width="1" height="1" /> -->
	</form>

// ### subscribe (old)
<!--
	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
		<input type="hidden" name="cmd" value="_s-xclick">
		<input type="hidden" name="hosted_button_id" value="Y99N6KF7G7HY4">
		
		<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
		<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
		
		<button id="form-btn" type="submit" class="btn btn-primary">Opt in</button>
	</form>
	-->

/// ### subscribe Smart
	<!-- 
		<div id="paypal-button-container"></div>
	-->
	
	<script src="https://www.paypal.com/sdk/js?client-id=%{pp_client}&vault=true&intent=subscription" data-sdk-integration-source="button-factory"></script>
	<script>
	// https://developer.paypal.com/docs/subscriptions/integrate/#4-create-a-subscription
	// https://github.com/paypal/paypal-sdk-client
	// sandbox ClientID = sb
	// sandbox Plan = P-2UF78835G6983425GLSM44MA
	//
	// live ClientID = ATAcrxtPhHde7QXkCGMOYJXg6xl1OYSadcB5nVKOG7SOM9l8NKTLmrkaOc1PNKFGIRW53CRaBJ5YlD1h
	// live plan = P-3KP735650X166604RL6NITYQ
	//
	// Later can add "locale" to the above init script definition
	/*
	paypal.Buttons({
		style: {
			shape: 'pill',
			color: 'blue',
			layout: 'vertical',
			label: 'subscribe'
		},
		createSubscription: function(data, actions) {
			return actions.subscription.create({
				'plan_id': 'P-3KP735650X166604RL6NITYQ'
			});
		},
		onApprove: function(data, actions){
			
			$("#sub-msg").html(
				'<h4 class="alert-heading">Thanks a lot!</h4>'
				+ '<p>We going to verify subscription and update your account details soon.Enjoy your ride.'
				+ '<br />In case of any issues - please leave a feedback, including your email and PayPal username</p>'
			);
			$("#sub-msg").addClass("alert-success");
			
			var req = $.post({
				url: "/api/verify-sub",
				data: JSON.stringify(data),
				contentType: 'application/json'
			})
			.done(function(result){
				
				console.log('vefiry - done - result = %j', result);
				
				setTimeout(location.reload, 1000);
				
			})
			.fail(function(err){
				console.log('vefiry - catch - err = %j', err);
			});
			
		}
	}).render('#paypal-button-container');
	*/
	</script>

// ### PAYPAL - IPN SAMPLE
POST http://localhost:9000/api/hook/ipn-sand/u5io3upa26bogdympqw3
Content-type = application/json

{
    "mc_gross": "1.00",
    "protection_eligibility": "Eligible",
    "payer_id": "DDA8PDATHWP3E",
    "payment_date": "00:27:28 Nov 03, 2020 PST",
    "charset": "windows-1252",
    "first_name": "test",
    "mc_fee": "0.33",
    "notify_version": "3.9",
    "payer_status": "verified",
    "business": "seller@mail.com",
    "quantity": "1",
    "verify_sign": "AE-lVFKZSDLmINp44zzA82FruzrMAFaXW-8htz0nwX0FFxS2Ts6bEV91",
    "txn_id": "9V855045MU3328105",
    "payment_type": "instant",
    "last_name": "buyer",
    "receiver_email": "seller@mail.com",
    "shipping_discount": "0.00",
    "receiver_id": "FVUNVUEM87CBW",
    "insurance_amount": "0.00",
    "txn_type": "web_accept",
    "item_name": "test",
    "discount": "0.00",
    "item_number": "",
    "residence_country": "FI",
    "test_ipn": "1",
    "shipping_method": "Default",
    "transaction_subject": "user=123test",
	"payment_status": "Completed",
	"payer_email": "buyer@mail.com",
	"custom": "userid=477b20b9c15969bda1a924125941516d",
	"payment_fee": "0.33",
	"payment_gross": "1.00",
	"mc_currency": "USD",
    "ipn_track_id": "e5b87df521f50"
}



