
var cloudRun = (
	process.env.CLOUD_RUN === 'true'
	|| process.env.NODE_ENV === 'production'
	|| (process.env.VCAP_APPLICATION && process.env.VCAP_SERVICES) 	// Cloud foundry
	|| process.env.AWS_EXECUTION_ENV === "AWS_ECS_FARGATE"			// AWS Fargate
	|| process.env.KUBERNETES_SERVICE_HOST							// Kubernetes
);
var appUrl = 'localhost';
var appPort = Number(process.env.PORT || 9000);

if( cloudRun ){
	if( process.env.APP_URL ){
		appUrl = process.env.APP_URL;
	}
	else if( process.env.VCAP_APPLICATION ){
		var vcapAppObj = JSON.parse(process.env.VCAP_APPLICATION);
		appUrl = vcapAppObj.uris[0];
	}
	else {
		appUrl = process.env.HOSTNAME;
	}
}
else {
	var secret = require( './secret.json' );
	for( var s in secret ){
		if( typeof(process.env[s]) === 'undefined' ){
			process.env[s] = secret[s];
		}
	}
	
	appUrl = 'http://localhost:' + appPort;
}

var config = {
	PORT: appPort,
	CLOUD_RUN: cloudRun,
	APP_URL: appUrl,
	GOOGLE_ID: process.env.GOOGLE_ID,
	GOOGLE_SECRET: process.env.GOOGLE_SECRET,
	SESSION_SECRET: process.env.SESSION_SECRET,
	DB_HOST: process.env.DB_HOST,
	DB_PORT: process.env.DB_PORT || 27017,
	DB_USER: process.env.DB_USER,
	DB_PASS: process.env.DB_PASS,
	DB_URL: process.env.DB_URL,
	DB_NAME: process.env.DB_NAME || 'sd',
	DB_TABLE_USER: process.env.DB_NAME_USER || 'sd-users',
	DB_TABLE_FEED: process.env.DB_TABLE_FEED || 'sd-feedback',
	DB_TABLE_PAYMENT: process.env.DB_TABLE_PAYMENT || 'sd-payments',
	HOOK_EVENT: process.env.HOOK_EVENT,
	SECURE_REDIRECT: process.env.SECURE_REDIRECT || 'true',
	PAYPAL_CLIENT: process.env.PAYPAL_CLIENT || 'ATAcrxtPhHde7QXkCGMOYJXg6xl1OYSadcB5nVKOG7SOM9l8NKTLmrkaOc1PNKFGIRW53CRaBJ5YlD1h',
	PAYPAL_SECRET: process.env.PAYPAL_SECRET
};

module.exports = config;




