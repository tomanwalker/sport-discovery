
var common = require('../lib/csv');

var file = 'olympics-modern.csv';

var data = common.readFile(file, ';');
//var dataBin = common.readFile('olympics-bin.csv', ';');


var mapper = function(x){
	
	x.height = Number(x.height);
	x.weight = Number(x.weight);
	x.year = Number(x.year);
	return x;
};

var sorter = function(a, b){
	if(a.name < b.name) { return -1; }
	if(a.name > b.name) { return 1; }
	return (
		a.year - b.year
	);
};

data = data.map( mapper ).filter( x => x.height > 0 && x.weight > 0 );
//dataBin = dataBin.map( mapper );

//data = data.sort(sorter);
//dataBin = dataBin.sort( sorter );

//console.log(debug.length);
//console.log(debug.slice(0,5));

//&& x.weight > 10 && x.height === 175
//x.year === '1996'
var fil = data.filter( x => x.sport === 'handball' && x.sex === 'female' && x.year === 1996 && x.height === 180 );
console.log('----------');
console.log(fil );
console.log('----------');
console.log(fil.length );

var avgWeight = fil.map( x => x.weight ).reduce((a,b) => a+b) / fil.length;
var avgHeight = fil.map( x => x.height ).reduce((a,b) => a+b) / fil.length;
console.log(fil.map( x => x.weight ) );
console.log('avg - w = %s | h = %s ', avgWeight, avgHeight);

//common.writeFile(file, data);
//common.writeFile('olympics-bin.csv', dataBin);


