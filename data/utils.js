

var ns = {};
module.exports = ns;

// ## func 
ns.leading = function(num, len){
	if( typeof(len) === 'undefined' ){
		len = 2;
	}
	
	return ("0".repeat(len) + num).slice(-len);
};

ns.dateformat = function(dateObj, fmt){
	
	var d = ns.leading(dateObj.getDate());
	var m = ns.leading(dateObj.getMonth() + 1);
	var y = dateObj.getFullYear();
	
	return [d, m, y].join('/');
};

ns.convertPounds = function(num){
	// pounds to kg
	return Math.round(num / 2.2046);
};

ns.convertFeet = function(arr){
	// arr = 5-9
	// feet and inches to CM
	return Math.round((Number(arr[0]) * 0.3048 + Number(arr[1]) * 0.0254) * 100);
};

ns.calculateBody = function(h, w){
	if( h < 20 ){
		return "";
	}
	if( w < 2 ){
		return "";
	}
	
	return Math.round( w / Math.pow(h/100, 2) * 10) / 10;
};

ns.nameShort = function(full){
	
	var sp = full.split(' ');
	var name = '';
	
	var rule1 = (
		(sp.length >= 3 || sp.length <= 4 )
		&& sp[0].length > 2
		&& sp[sp.length-1].length > 2
		&& sp[sp.length-1].indexOf(')') === -1
		&& sp[sp.length-1].indexOf('.') === -1
	);
	
	var rule2 = ( sp.length === 2 );
	
	var rule3 = (
		sp[sp.length-1].indexOf('.') !== -1
		|| sp[sp.length-1].indexOf(')') !== -1
	);
	
	if( rule1 || rule2 ){
		name = sp[0] + ' ' + sp[sp.length-1];
	}
	else if( rule3 ){
		name = sp[0] + ' ' + sp[sp.length-2];
	}
	else {
		name = full;
	}
	
	return name;
};



