
// ## dependencies
var csv = require('../../lib/csv');

var data = csv.readFile('../nba.csv');

var LIMIT = 180;

var fil = data.filter(function(x){
	
	return (
		Number(x.height) <= LIMIT
		&& !x.sub.includes('pg')
		&& !x.sub.includes('sg')
	);
	
}).sort(function(a,b){
	return (
		Number(b.medal_number) - Number(a.medal_number)
	);
});


var show = fil.map(function(x){
	return x.name;
});
console.log(show);



