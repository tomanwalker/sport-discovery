
// ## dependencies
var rp = require('request-promise');
var html2json = require('html2json').html2json;

var csv = require('../../lib/csv');
var utils = require('../utils');

// ## config
var SEASON_BASE_URL = 'https://www.basketball-reference.com/playoffs/NBA_{year}.html';
//var FINALS_MIN = 1980;
//var FINALS_MAX = 2020;
var FINALS_MIN = 2000;
var FINALS_MAX = 2000;

var teamTable = {};

// ## func
var loadTeams = function(){
	
	var teamList = csv.readFile('../nba-teams.csv');
	teamList.forEach(function(x){
		teamTable[x.name] = x.ab;
	});
};

var getFinals = async function(){
	console.log('getFinals - start...');
	
	var finalsList = [];
	for(var i=FINALS_MIN; i<=FINALS_MAX; i++){
		var url = SEASON_BASE_URL.replace('{year}', i);
		var obj = {year: i};
		
		var resHtml = await rp(url);
		var rootJson = html2json(resHtml);
		var bodyJson = rootJson.child.find( x => x.child ).child.find( x => x.tag === 'body' );
		var wrapJson = bodyJson.child.find( x => x.tag === 'div' && x.attr && x.attr.id === 'wrap' );
		var infoJson = wrapJson.child.find( x => x.tag === 'div' && x.attr && x.attr.id === 'info' );
		var metaJson = infoJson.child.find( x => x.tag === 'div' && x.attr && x.attr.id === 'meta' );
		var secondInfoDiv = metaJson.child.filter( x => x.tag === 'div' )[1];
		var infoPar = secondInfoDiv.child.filter( x => x.tag === 'p' );
		
		var parChamp = infoPar.find( x => x.child.find(z => z.tag === 'strong' && z.child.find(s => s.text === 'League Champion') ) );
		//var parMvp = infoPar.find( x => x.child.find(z => z.tag === 'strong' && z.child.find(s => s.text === 'Most Valuable Player') ) );
		
		//console.log(JSON.stringify(parChamp));
		//console.log(JSON.stringify(parMvp));
		
		var champLong = parChamp.child.find( x => x.tag === 'a' ).child.find( x => x.node === "text" ).text.trim();
		obj.champ = teamTable[champLong]; 
		//obj.mvp = parMvp.child.find( x => x.tag === 'a' ).child.find( x => x.node === "text" ).text.trim();
		
		var contentJson = wrapJson.child.find( x => x.tag === 'div' && x.attr && x.attr.id === 'content' );
		var playoffDiv = contentJson.child.find( x => x.tag === 'div' && x.attr && x.attr.id === 'all_all_playoffs');
		var tableDiv = playoffDiv.child.filter( x => x.tag === 'div' )[1].child.find( x => x.attr && x.attr.id === 'div_all_playoffs');
		var tableEl = tableDiv.child.find( x => x.tag === 'table' );
		var tableBodyEl = tableEl.child.find( x => x.tag === 'tbody' );
		
		console.log(tableBodyEl);
		
		finalsList.push(obj);
	}
	console.log('--------');
	console.log(finalsList);
};
var main = async function(){
	
	loadTeams();
	await getFinals();
	
};

main();


