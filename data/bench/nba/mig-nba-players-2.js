
// ## dep
var csv = require('../../../lib/csv');

// ## conf
var YEAR_LIMIT = 1980;
var YEAR_FINAL = 2018;


// ## fun


// ## flow
var playerData = csv.readFile('nba-sample-1.csv');
var mvpData = csv.readFile('nba-agg-mvp.csv');
var champData = csv.readFile('nba-agg-champ.csv');

var sel = playerData.filter(function(x){
	return Number(x.year_end) >= YEAR_LIMIT;
});

console.log('filter - %s / %s', sel.length, playerData.length);

sel = sel.map(function(x){
	
	delete x.team;
	
	x.mvp_count = 0;
	x.gold = 0;
	x.silver = 0;
	x.bronze = 0;
	
	var wasMvp = mvpData.find(z => x.name === z.name);
	if( wasMvp ){
		x.mvp_count = Number(wasMvp.count);
	}
	
	return x;
});

var missRoster = [];
champData.forEach(function(row){
	try{
		var roster = csv.readFile('rosters/' + row.team + '.csv');
		roster = roster.map(function(x){
			x.from = Number(x.from);
			x.to = Number(x.to);
			return x;
		}).filter(function(x){
			return x.to >= YEAR_LIMIT;
		});
		
		var gold_years = row.gold_years.split(',');
		var silver_years = row.silver_years.split(',');
		var bronze_years = row.bronze_years.split(',');
		
		var forLoop = function(colore){
			return function(g){
				var participants = roster.filter(function(p){
					return (
						p.from <= g
						&& p.to >= g
						&& Number(p.games) > 20
						&& Number(p.minutes) > 100
					);
				});
				
				participants.forEach(function(p){
					var found = sel.find(x => x.name === p.player);
					if( found ){
						//console.log(found);
						found[colore] += 1;
					}
				});
			};
		};
		
		gold_years.forEach(forLoop('gold'));
		silver_years.forEach(forLoop('silver'));
		bronze_years.forEach(forLoop('bronze'));
		
	}
	catch(err){
		console.log('missing roster - %s', row.team);
		missRoster.push( row.team );
	}
});

sel = sel.map(function(x){
	
	// being MVP is like Olympic gold
	// being champ is 4 times less (100/4 = 25)
	x.medal_number = x.mvp_count * 100 + x.gold * 25 + x.silver * 2.5 + x.bronze * 0.25;
	
	return x;
	
}).sort(function(a,b){
	var diff1 = b.medal_number - a.medal_number; // DESC
	if( diff1 !== 0 ){
		return diff1;
	}
	return Number(a.year_start) - Number(b.year_start); // ASC
});


console.log('------');
var debug = sel.filter( x => x.medal_number > 0 );
//console.log(debug.slice(0, 1));
console.log( 'miss - %s', JSON.stringify(missRoster) );
console.log('------');
console.log('length = %s / %s', debug.length, sel.length);

csv.writeFile('nba-sample-2.csv', sel);




