
// ## dependencies
var csv = require('../../lib/csv');
var utils = require('../utils');

// ## flow
console.log('migration - nba 1950');

var file1 = 'nba-1950/player_data.csv'; // position & dob

var data1 = csv.readFile(file1, ',');
var data2 = csv.readFile('../olympics-modern.csv', ';');

var YEAR_LIMIT = 1970;

var data3 = data2.filter( x => x.sport === 'basketball' );

/*
//nba-1950/player_data.csv
readFile - loaded - sample = {
	"name":"Alaa Abdelnaby","year_start":"1991","year_end":"1995","position":"F-C",
	"height":"6-10","weight":"240","birth_date":"June 24, 1968","college":"Duke University"
}
*/

// mapping / join / year filter
var pos = {
	'C': 'center',
	'C-F': 'center',
	'F': 'forward',
	'F-C': 'forward',
	'F-G': 'forward',
	'G': 'guard',
	'G-F': 'guard'
};
console.log('------------');
var mapped = [];
data1.forEach(function(x){
	
	var end = Number(x.year_end);
	
	if( end < YEAR_LIMIT ){
		return false;
	}
	
	var d = new Date(x.birth_date);
	
	var obj = {
		name: x.name,
		full: x.name,
		sport: 'basketball',
		country: 'USA',
		sex: 'male',
		date_of_birth: utils.dateformat(d),
		year_start: Number(x.year_start),
		year_end: end
	};
	
	var hImp = x.height.split('-');
	var wImp = Number(x.weight);
	
	var approxH = utils.convertFeet(hImp);
	var approxW = utils.convertPounds(wImp);
	
	obj.height = approxH;
	obj.weight = approxW;
	obj.bmi = utils.calculateBody(approxH, approxW);
	
	if( obj.height < 100 || isNaN(obj.height) ){
		console.log('wrong h = %j', obj);
	}
	if( obj.weight < 50 || isNaN(obj.weight) ){
		console.log('wrong w = %j', obj);
	}
	
	obj.sub = pos[x.position];
	
	var checkName = data3.filter(z => z.name === x.name );
	if( checkName.length ){
		obj.full = checkName[0].full;
		obj.country = checkName[0].country;
		
		// update olympics - sub, dob, height, weight
		var olympAtt = ['sub', 'date_of_birth', 'height', 'weight'];
		checkName.forEach(function(k){
			olympAtt.forEach(function(a){
				k[a] = obj[a];
			});
			
			k.bmi = utils.calculateBody(k.height, k.weight);
		});
		
		
	}
	
	// TODO - Year split
	//
	// TODO - team mapping
	// https://www.basketball-reference.com/teams/LAL/1970.html
	// https://www.basketball-reference.com/teams/LAL/2004.html
	//
	// TODO - Find out champ / medal_number
	// /players/j/
	
	
	obj.team = '';
	
	mapped.push(obj);
});

console.log('------------');
console.log('mapped - len = %s', mapped.length);
console.log('------------');
var sampleNames = ['Allen Iverson', 'Michael Jordan', 'Isaiah Thomas'];

sampleNames.forEach(function(s){
	console.log('mapped - sample = %j', mapped.find( x => x.name === s ) );
});


//csv.writeFile('nba-sample-1.csv', mapped);
//csv.writeFile('../olympics-modern.csv', data2);

