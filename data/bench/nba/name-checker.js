

// ## dep
var csv = require('../../../lib/csv');
var fs = require('fs');


var FOLDER = './rosters/';
var repTable = {
	'ğ': 'g',
	'ü': 'u',
	'ū': 'u',
	'á': 'a',
	'í': 'i',
	'ó': 'o',
	'é': 'e',
	'ž': 'zh',
	'č': 'ch',
	'ć': 'c',
	'İ': 'I',
	'ņ': 'n', 
	'ģ': 'g',
	'š': 'sh',
	'İ': 'I',
	'ņ': 'n',
	'Ö': 'O',
	'ş': 's',
	'Ž': 'ZH',
	'ã': 'a',
	'Š': 'Sh',
	'ı': 'i',
	'ô': 'o',
	'ā': 'a',
	'Á': 'A',
	'ń': 'n',
	'ê': 'e'
};
/*

FOUND = CLE.csv | len = 7 | sample = Martynas Andriuškevicius
FOUND = DET.csv | len = 1 | sample = Željko Rebraca
FOUND = GSW.csv | len = 1 | sample = Anderson Varejão
FOUND = HOU.csv | len = 5 | sample = Ömer Aşık
FOUND = NYK.csv | len = 2 | sample = Bruno Šundov
FOUND = PHI.csv | len = 1 | sample = Dario Šaric

*/


fs.readdir(FOLDER, function(err, files){
	files.forEach(function(file){
		var data = csv.readFile(FOLDER + file);
		var fil = data.filter(function(x){
			return x.player.match(/[^\x00-\x7F]+/);
		});
		if( fil.length === 0 ){
			return false;
		}
		console.log('FOUND = %s | len = %s | sample = %s', file, fil.length, (fil[0] && fil[0].player) );
		
		fil.forEach(function(p){
			
			Object.keys(repTable).forEach(function(c){
				if( p.player.includes(c) ){
					p.player = p.player.replace(c, repTable[c]);
				}
			});
			
		});
		
		csv.writeFile(FOLDER + file, data);
	});
});


