
// ## dep
var csv = require('../../../lib/csv');
var toolbox = require('../../../lib/toolbox');

var data = csv.readFile('../nba.csv');

var heightMap = {};
var stats = {};

data.forEach(function(x){
	
	x.height = Number(x.height);
	x.medal_number = Number(x.medal_number);
	
	if( !stats[x.sub] ){
		stats[x.sub] = {
			pos: x.sub,
			cnt: 0,
			min: 300,
			max: 0,
			low: 0,
			mid: 0,
			high: 0
		};
		
		heightMap[x.sub] = [];
	}
	
	stats[x.sub].cnt += 1;
	stats[x.sub].min = Math.min(stats[x.sub].min, x.height);
	stats[x.sub].max = Math.max(stats[x.sub].max, x.height);
	
	var pushTimes = 1;
	if( x.medal_number > 0 ){
		pushTimes += 1;
	}
	if( x.medal_number >= 10 ){
		pushTimes += 1;
	}
	if( x.medal_number >= 100 ){
		pushTimes += 2;
	}
	
	for(var i=0; i< pushTimes; i++){
		heightMap[x.sub].push(x.height);
	}
	
});

Object.keys(stats).forEach(function(k){
	
	heightMap[k] = heightMap[k].sort();
	
	stats[k].low = toolbox.analytics.pct(heightMap[k], 0.05, true);
	stats[k].mid = toolbox.analytics.pct(heightMap[k], 0.50, true);
	stats[k].high = toolbox.analytics.pct(heightMap[k], 0.95, true);
	
});


console.log(stats);

return process.exit(0);



