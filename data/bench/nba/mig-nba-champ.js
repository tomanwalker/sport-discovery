
var csv = require('../../lib/csv');

var teamList = csv.readFile('../nba-teams.csv');
var mvpData = csv.readFile('nba-mvp.csv');
var playoffData = csv.readFile('nba-playoff.csv');

var teamTable = {};
teamList.forEach(function(x){
	teamTable[x.name] = x.ab;
});

var extractTeamName = function(nameNscore){
	var t1 = nameNscore.split(' ');
	return t1.slice(0,-1).join(' ');
};
var finalTeams = playoffData.filter(function(x){
	return (
		x.Lg === 'NBA'
		&&
		x.Series.includes('Finals')
	);
}).map(function(x){
	
	var obj = {
		year: x.Yr,
		stage: x.Series
	};
	var t1 = extractTeamName(x['Team(won)']);
	var t2 = extractTeamName(x['Team(lost)']);
	
	obj.won = teamTable[t1];
	obj.lost = teamTable[t2];
	
	if( typeof(obj.won) === 'undefined' ){
		console.log('miss AB - ', t1);
	}
	if( typeof(obj.lost) === 'undefined' ){
		console.log('miss AB - ', t2);
	}
	
	return obj;
});

var mapped = {};
finalTeams.forEach(function(x){
	
	if( !mapped[x.year] ){
		mapped[x.year] = {
			year: x.year,
			mvp: mvpData.find(z => z.year === x.year).player
		};
	}
	
	if( x.stage === 'Finals' ){
		mapped[x.year].first = x.won;
		mapped[x.year].second = x.lost;
	}
	else {
		var attKey = 'third_' + x.stage[0].toLowerCase();
		mapped[x.year][attKey] = x.lost;
	}
});

var result = Object.keys(mapped).map(function(k){
	
	if( typeof(mapped[k].third_w) === 'undefined' ){
		console.log('miss TH - ', mapped[k]);
	}
	
	return mapped[k];
}).sort(function(a,b){
	return a.year - b.year; //ASC
});
console.log(result.slice(0,5));

csv.writeFile('nba-seasons.csv', result);




