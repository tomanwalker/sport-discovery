
// ## dep
var csv = require('../../../lib/csv');

// ## conf
var YEAR_LIMIT = 1980;


// ## fun


// ## flow
var playerData = csv.readFile('nba-sample-2.csv');

var ready = playerData.filter(function(x){
	
	return Number(x.year_start) >= YEAR_LIMIT;
	
}).sort(function(a,b){
	var diff1 = b.medal_number - a.medal_number; // DESC
	if( diff1 !== 0 ){
		return diff1;
	}
	return Number(a.year_start) - Number(b.year_start); // ASC
});


console.log('length input = %s', playerData.length);
console.log('length cut (%s) = %s ', ready.length, YEAR_LIMIT);

csv.writeFile('../../nba.csv', ready);




