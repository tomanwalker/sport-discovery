
var csv = require('../../lib/csv');

var seasons = csv.readFile('nba-seasons.csv');

var mvpMap = {};
var champMap = {};

//var groupper = function(key,
var teamInit = function(name){
	if( !champMap[name] ){
		champMap[name] = {
			team: name,
			yearFirst: [],
			yearSecond: [],
			yearThird: []
		};
	}
};

var year_min = 2200;
var year_max = 1900;
seasons.forEach(function(s){
	
	if( !mvpMap[s.mvp] ){
		mvpMap[s.mvp] = {
			name: s.mvp,
			yearList: []
		};
	}
	
	mvpMap[s.mvp].yearList.push(s.year);
	
	teamInit(s.first);
	teamInit(s.second);
	teamInit(s.third_e);
	teamInit(s.third_w);
	
	champMap[s.first].yearFirst.push(s.year);
	champMap[s.second].yearSecond.push(s.year);
	champMap[s.third_e].yearThird.push(s.year);
	champMap[s.third_w].yearThird.push(s.year);
	
	year_min = Math.min(year_min, s.year);
	year_max = Math.max(year_max, s.year);
	
});

var mapper = function(t){
	
	var m = t[1];
	m.count = m.yearList.length;
	m.last = m.yearList[m.count-1];
	m.years = m.yearList.join(',');
	delete m.yearList;
	return m;
	
};

var sorter = function(a,b){
	var diff = b.count - a.count; // DESC
	if( diff !== 0 ){
		return diff;
	}
	return b.last - a.last; // DESC
};

var mvpList = Object.entries(mvpMap).map(mapper).sort(sorter);
//var champList = Object.entries(champMap).map(mapper).sort(sorter);
var champList = Object.entries(champMap).map(function(t){
	
	var rec = t[1];
	var obj = {
		team: rec.team,
		year_start: year_min,
		year_end: year_max,
		number: 0,
		gold: rec.yearFirst.length,
		silver: rec.yearSecond.length,
		bronze: rec.yearThird.length,
		gold_years: rec.yearFirst.join(','),
		silver_years: rec.yearSecond.join(','),
		bronze_years: rec.yearThird.join(',')
	};
	
	obj.number = obj.gold * 100 + obj.silver * 10 + obj.bronze;
	
	return obj;
}).sort(function(a,b){
	return b.number - a.number; // DESC
});


console.log(champList.slice(0,3));
csv.writeFile('nba-agg-mvp.csv', mvpList);
csv.writeFile('nba-agg-champ.csv', champList);


