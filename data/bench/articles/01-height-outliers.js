
// ### deps
var csv = require('../../../lib/csv');
var fs = require('fs');

// ### conf
var time = new Date();
var ts = time.toISOString();

var banSport = ['rowing', 'baseball', 'field hockey', 'shooting', 'aquatics', 'wrestling',
	'softball', 'cycling', 'table tennis', 'equestrian', 'sledding',
	'badminton', 'ski jumping', 'nordic combined', 'archery', 'curling',
	'figure skating'];
var banSub = ['throw', 'rhythmic' ];

//var ignoreTall = ['basketball:na', 'volleyball:na', 'aquatics:swimming'];
//var ignoreShort = ['aquatics:diving', 'gymnastics:artistic' ];

var ignoreTall = [ 'basketball', 'volleyball', 'aquatics', 'ice hockey', 'handball', 
	'fencing', 'tennis', 'biathlon', 'skiing', 'taekwondo', 'boxing'];
var ignoreShort = [ 'gymnastics', 'weightlifting', 'judo', 'snowboarding' ];

var decrease = ['judo', 'boxing', 'speed skating', 'fencing', 'handball'];
var increase = ['basketball', 'gymnastics'];

// ### func


// ### flow

// load all the data
var data = csv.readFile('../../olympics-modern.csv');
data = data.concat( csv.readFile('../../nba.csv') );

data = data.map(function(x){
	
	x.height = Number(x.height);
	x.year = Number( x.year || x.year_start );
	x.medal_number = Number(x.medal_number);
	
	if( x.sub === '' || x.sub === 'none' ){
		x.sub = 'na';
	}
	
	return x;
	
}).filter(function(x){
	return (
		x.height > 10
		&& x.year > 1990
		&& !banSport.includes( x.sport )
		&& !banSub.includes( x.sub )
	);
});

console.log('-----');
console.log('loaded - sample - %j', data[113]);
console.log('-----');

// Map out Lists - metrics & people
var peopleMap = {};
var metricsMap = {};

data.forEach(function(x){
	// Filter People where medal > 0 & Group People by Full
	if( x.medal_number > 0 ){
		
		var personKey = [x.sport, x.country, x.sex, x.full].join('-');
		
		if( !peopleMap[personKey] ){
			peopleMap[personKey] = {
				name: x.name,
				full: x.full,
				sex: x.sex,
				sport: x.sport,
				sub: x.sub,
				country: x.country,
				height: x.height,
				medals: 0,
				h_diff: 0,
				val: 0,
				h_avg: 0,
				place: 0,
				appear: []
			};
		}
		
		peopleMap[personKey].medals += x.medal_number;
		peopleMap[personKey].appear.push( x );
	}
	
	// Group Sports by sport
	//var mKey = [x.sex, x.sport, x.sub].join('-');
	var mKey = [x.sex, x.sport].join('-');
	
	if( !metricsMap[mKey] ){
		metricsMap[mKey] = {
			label: mKey,
			sport: x.sport,
			sex: x.sex,
			sub: x.sub,
			cnt: 0,
			h_sum: 0
		}
	}
	
	metricsMap[mKey].cnt += 1;
	metricsMap[mKey].h_sum += x.height;
	metricsMap[mKey].h_avg = Math.round( metricsMap[mKey].h_sum / metricsMap[mKey].cnt * 100 ) / 100;
	
});

console.log('metrics.len = %s', Object.keys(metricsMap).length);
console.log(metricsMap['male-basketball']);
console.log('-----');

// ForEach Person - run raft against the one sport
var peopleList = Object.keys(peopleMap).map(function(k){
	return peopleMap[k];
}).filter(function(x){
	return x.medals > 101;
});

console.log('people.len = %s', peopleList.length);
console.log('people.sample = %j', peopleList.find(x => x.name.includes('Iverson')) );
console.log('people.sample = %j', peopleList.find(x => x.name.includes('Nemov')) );

// Sort by Diff
var diffSort = function(a,b){
	
	var diff1 = Math.round(b.val) - Math.round(a.val); // DESC
	if( diff1 !== 0 ){
		return diff1;
	}
	
	return b.medals - a.medals;
	
};

peopleList = peopleList.map(function(x){
	
	//var mKey = [x.sex, x.sport, x.sub].join('-');
	var mKey = [x.sex, x.sport].join('-');
	
	var targetSport = metricsMap[mKey];
	
	// var ignoreTall = ['basketball:na', 'volleyball:na'];
	// var ignoreShort = ['aquatics:diving'];
	
	//var sportTag = [x.sport, x.sub].join(':');
	var sportTag = x.sport;
	
	x.h_avg = targetSport.h_avg;
	x.h_diff = x.height - targetSport.h_avg;
	x.val = Math.abs( x.h_diff );
	
	
	if( ignoreTall.includes( sportTag ) && x.h_diff > 0 ){
		x.val = 0;
	}
	if( ignoreShort.includes( sportTag ) && x.h_diff < 0 ){
		x.val = 0;
	}
	
	if( decrease.includes( x.sport ) ){
		x.val = x.val * 0.5;
	}
	if( increase.includes( x.sport ) ){
		x.val = x.val * 2;
	}
	
	return x;
	
}).filter(function(x){
	
	return x.val > 5;
	
}).sort(diffSort);

for(var i=0; i<peopleList.length; i++){
	peopleList[i].place = (i+1);
}

console.log('-----');

//console.log('people.sample = %j', peopleList.find(x => x.name.includes('Iverson')) );
var onePerSport = {};
peopleList.forEach(function(x){
	
	var sKey = [x.sex, x.sport].join('-');
	
	if( !onePerSport[sKey] ){
		onePerSport[sKey] = x;
	}
	
});

var selected = Object.keys(onePerSport).map(function(k){
	return onePerSport[k];
}).sort(diffSort);

//console.log( 'DEBUG = %j', onePerSport );
//console.log('selected = %j', selected.slice(0, 10) );

console.log( 'DEBUG = %s', selected.length );
fs.writeFileSync('test.json', JSON.stringify(selected, null, 2));


/*
peopleList.slice(0, 10).forEach(function(x){
	
	var toPrint = [];
	for(var p in x ){
		toPrint.push(p + ' = ' + x[p]);
	}
	
	console.log(toPrint.join(' | '));
});
*/

