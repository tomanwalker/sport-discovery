
// ## deps
var csv = require('../../../lib/csv');
var fs = require('fs');

// ### conf
var time = new Date();
var ts = time.toISOString();

var FIRST_YEAR = 1980;
var TOP_NUM = 2;

var YEAR_FROM = 1980;
var YEAR_TO = 2018;

// ## func
var teamSorter = function(a, b){
	
	var numDiff = Number(b.number) - Number(a.number);
	if( numDiff !== 0 ){
		return numDiff;
	}
	
	return b.bronze_years[0] - a.bronze_years[0];
};
var champMapper = function(x){
	
	var sp_g = x.gold_years.split(',');
	var sp_s = x.silver_years.split(',');
	var sp_b = x.bronze_years.split(',');
	
	x.gold_years = sp_g.map(n => Number(n) ).filter(n => n >= FIRST_YEAR).sort();
	x.silver_years = sp_s.map(n => Number(n) ).filter(n => n >= FIRST_YEAR).sort();
	x.bronze_years = sp_b.map(n => Number(n) ).filter(n => n >= FIRST_YEAR).sort();
	
	x.year_start = FIRST_YEAR;
	
	x.gold = x.gold_years.length;
	x.silver = x.silver_years.length;
	x.bronze = x.bronze_years.length;
	x.number = x.gold * 100 + x.silver * 10 + x.bronze;
	
	return x;
};
var getStandardDeviation = function(array, n, mean) {
	if( typeof(n) === 'undefined' ){
		n = array.length;
	}
	if( typeof(mean) === 'undefined' ){
		mean = array.reduce((a, b) => a + b) / n;
	}
	return Math.sqrt(array.map(x => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / n);
};

// ## load data (nba / champ)
var data = csv.readFile('../../nba.csv');
var champ = csv.readFile('../nba/nba-agg-champ.csv').map(champMapper);

console.log('------------');
var topTeams = champ.sort(teamSorter).slice(0, TOP_NUM);
console.log('best = %j', topTeams);
var worstTeams = champ.sort(teamSorter).slice(-TOP_NUM);
console.log('worst = %j', worstTeams);
console.log('------------');

//var selectedTeams = topTeams.concat(worstTeams);
var selectedTeams = champ.sort(teamSorter);
var listObj = [];
var missing = [];

// ## for the last few years - collect Mean/StdDev
selectedTeams.forEach(function(t){
	
	// {"player":"Mark Acres","from":"1993","to":"1993","years":"1","games":"12","minutes":"246"}
	var teamData = null;
	try{
		var teamData = csv.readFile('../nba/rosters/' + t.team + '.csv');
	}
	catch(e){
		console.log('data miss = %s', t.team);
		missing.push({t: t.team, n: t.number});
		return false;
	}
	
	var roster = teamData.map(function(p){
		
		p.from = Number(p.from);
		p.to = Number(p.to);
		p.minutes = Number(p.minutes);
		
		var found = data.find(x => x.name === p.player);
		if( found ){
			p.height = Number(found.height);
			p.weight = Number(found.weight);
			p.bmi = Number(found.bmi);
		}
		
		return p;
		
	}).filter(function(p){
		return p.minutes > 20;
	});
	
	console.log('====');
	console.log('roster %s = %j', t.team, roster[0]);
	console.log('====');
	
	t.statMap = {};
	
	for( var y=YEAR_FROM; y<=YEAR_TO; y++){
		
		var fil = roster.filter(function(x){
			return (
				y <= Number(x.to)
				&& y >= Number(x.from)
			);
		});
		
		var here_gold = t.gold_years.includes( y );
		var here_silver = t.silver_years.includes( y );
		var here_bronze = t.bronze_years.includes( y );
		var here_number = Number(here_gold) * 3 + Number(here_silver) * 2 + Number(here_bronze);
		
		var h_arr = fil.map(x => (x.height || 0)).filter( x => x > 0 );
		var w_arr = fil.map(x => (x.weight || 0)).filter( x => x > 0 );
		
		var h_cnt = h_arr.length;
		if( h_cnt > 0 ){
			var h_sum = h_arr.reduce((a,b) => a + b);
			var h_avg = Math.round(h_sum / h_cnt * 100) / 100;
			var h_std = Math.round(getStandardDeviation(h_arr, h_cnt, h_avg) * 100) / 100;
			
			var w_sum = w_arr.reduce((a,b) => a + b);
			var w_avg = Math.round(w_sum / w_arr.length * 100) / 100;
			var w_std = Math.round(getStandardDeviation(w_arr) * 100) / 100;
			
			var obj = {
				year: y,
				team: t.team,
				number: here_number,
				cnt: fil.length,
				h_cnt: h_cnt,
				h_avg: h_avg,
				h_std: h_std,
				w_avg: w_avg,
				w_std: w_std
			};
			
			listObj.push(obj);
		
		}
	}
	
});

console.log('********');
console.log('enriched = %j', listObj[0]);
console.log('********');

// ## make a Champ table with Div
csv.writeFile('02-div-test.csv', listObj);

// ## calc correlations (avg, std)
console.log(
	missing.sort((a,b) => b.n - a.n )
);


