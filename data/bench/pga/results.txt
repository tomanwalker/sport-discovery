https://en.wikipedia.org/wiki/List_of_golfers_with_most_PGA_Tour_wins

The list is complete as of September 13, 2020.[1]

Rank 	Name[a] 	Year of birth
and death 	Country 	Wins 	Majors[b] 	Winning
span 	Span
(years)
1 	Sam Snead H 	1912–2002 	 United States 	82 	7 	1936–1965 	30
Tiger Woods H 	1975– 	 United States 	15 	1996–2019 	24
3 	Jack Nicklaus H 	1940– 	 United States 	73 	18 	1962–1986 	25
4 	Ben Hogan H 	1912–1997 	 United States 	64 	9 	1938–1959 	22
5 	Arnold Palmer H 	1929–2016 	 United States 	62 	7 	1955–1973 	19
6 	Byron Nelson H 	1912–2006 	 United States 	52 	5 	1935–1951 	17
7 	Billy Casper H 	1931–2015 	 United States 	51 	3 	1956–1975 	20
8 	Walter Hagen H 	1892–1969 	 United States 	45 	11 	1914–1936 	23
9 	Phil Mickelson H 	1970– 	 United States 	44 	5 	1991–2019 	29
10 	Cary Middlecoff H 	1921–1998 	 United States 	39 	3 	1945–1961 	17
Tom Watson H 	1949– 	 United States 	8 	1974–1998 	25
12 	Gene Sarazen H 	1902–1999 	 United States 	38 	7 	1922–1941 	20
13 	Lloyd Mangrum H 	1914–1973 	 United States 	36 	1 	1940–1956 	17
14 	Vijay Singh H 	1963– 	 Fiji 	34 	3 	1993–2008 	16
15 	Jimmy Demaret H 	1910–1983 	 United States 	31 	3 	1938–1957 	20
16 	Horton Smith H 	1908–1963 	 United States 	30 	2 	1928–1941 	14
17 	Harry Cooper H 	1904–2000 	 United States[c] 	29 	0 	1923–1939 	17
Gene Littler H 	1930–2019 	 United States 	1 	1954–1977 	24
Lee Trevino H 	1939– 	 United States 	6 	1968–1984 	17
20 	Leo Diegel H 	1899–1951 	 United States 	28 	2 	1920–1934 	15
Paul Runyan H 	1908–2002 	 United States 	2 	1930–1941 	12
22 	Henry Picard H 	1906–1997 	 United States 	26 	2 	1932–1945 	14
23 	Tommy Armour H 	1894–1968 	 Scotland
 United States 	25 	3 	1920–1938 	19
Johnny Miller H 	1947– 	 United States 	2 	1971–1994 	24
25 	Gary Player H 	1935– 	 South Africa 	24 	9 	1958–1978 	21
Macdonald Smith 	1892–1949 	 Scotland
 United States 	0 	1912–1936 	25
27 	Dustin Johnson 	1984– 	 United States 	23 	1 	2008–2020 	13
28 	Jim Barnes H 	1886–1966 	 England
 United States[d] 	22 	4 	1914–1937 	24
Johnny Farrell 	1901–1988 	 United States 	1 	1921–1936 	16
Raymond Floyd H 	1942– 	 United States 	4 	1963–1992 	30
31 	Davis Love III H 	1964– 	 United States 	21 	1 	1987–2015 	29
Willie Macfarlane 	1890–1961 	 Scotland
 United States 	1 	1916–1936 	21
Lanny Wadkins H 	1949– 	 United States 	1 	1972–1992 	21
Craig Wood H 	1901–1968 	 United States 	2 	1928–1944 	17
35 	Hale Irwin H 	1945– 	 United States 	20 	3 	1971–1994 	24
Greg Norman H 	1955– 	 Australia 	2 	1984–1997 	14
Johnny Revolta 	1911–1991 	 United States 	1 	1933–1944 	12
Doug Sanders 	1933–2020 	 United States 	0 	1956–1972 	17
39 	Ben Crenshaw H 	1952– 	 United States 	19 	2 	1973–1995 	23
Ernie Els H 	1969– 	 South Africa 	4 	1994–2012 	19
Doug Ford H 	1922–2018 	 United States 	2 	1952–1963 	12
Hubert Green H 	1946–2018 	 United States 	2 	1971–1985 	15
Tom Kite H 	1949– 	 United States 	1 	1976–1993 	18
Bill Mehlhorn 	1898–1989 	 United States 	0 	1923–1930 	8
45 	Julius Boros H 	1920–1994 	 United States 	18 	3 	1952–1968 	17
Jim Ferrier 	1915–1986 	 Australia
 United States 	1 	1944–1961 	18
Dutch Harrison 	1910–1982 	 United States 	0 	1939–1958 	20
Rory McIlroy 	1989– 	 Northern Ireland 	4 	2010–2019 	10
Nick Price H 	1957– 	 Zimbabwe 	3 	1983–2002 	20
50 	Bobby Cruickshank 	1894–1975 	 Scotland
 United States 	17 	0 	1921–1936 	16
Jim Furyk 	1970– 	 United States 	1 	1995–2015 	21
Harold "Jug" McSpaden 	1908–1996 	 United States 	0 	1933–1945 	13
Curtis Strange H 	1955– 	 United States 	2 	1979–1989 	11 