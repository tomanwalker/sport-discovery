
// ## dependencies
var fs = require('fs');

var csv = require('../../lib/csv');
var toolbox = require('../../lib/toolbox');

// ## config
var profile = {
	kind: 'kid',
	gender: 'male',
	mob: 12,
	p_h_1: 175,
	p_h_2: 166
};

var REPORT_NAME_DOB = '../reports/dob-presense.json';
var REPORT_NAME_MONTH_DISTRO = '../reports/distro-month.json';

var time = new Date();
var ts = time.toISOString();

// ## func
var mapFunc = function(x){
	
	x.height = Number(x.height);
	x.bmi = Number(x.bmi);
	
	x.month = Number( x.date_of_birth.split('/')[1] );
	return x;
	
};
var filterFunc = function(x){
	return (
		x.height && x.height > 5
		&& x.bmi && x.bmi > 5
		&& x.month && x.month > 0
	);
};

var randomBetween = function(min, max) { 
	// min and max included 
	return Math.floor(Math.random() * (max - min + 1) + min);
};

var filterGender = function(x){
	
	var check = x.sex === profile.gender;
	return check;
	
};

// ## flow
var data1 = csv.readFile('../olympics-modern.csv');
var data2 = csv.readFile('../nba.csv');

var total = data1.length + data2.length;

var mapped = data1.map(mapFunc);
mapped = mapped.concat( data2.map(mapFunc) );

console.log('============');
console.log('mapped.len = %s / %s / %s %', mapped.length, total, Math.round(mapped.length / total * 100) );
console.log('mapped.sample = %j', mapped[randomBetween(0, mapped.length)]);

var fil = mapped.filter(filterFunc).filter(filterGender);
console.log('fil.len = %s / %s %', fil.length, Math.round(fil.length / total * 100) );
console.log('fil.sample = %j', fil[randomBetween(0, fil.length)]);

// group by Sport and build up monthList
var sportGroup = {};
var groupStats = {};
var calcTable = {};

fil.forEach(function(x){
	
	var k = x.sport;
	
	if( !sportGroup[k] ){
		sportGroup[k] = [];
		groupStats[k] = {
			label: k,
			cnt: 0,
			monthMin: 100,
			monthMax: 0,
			monthTableCnt: {},
			monthTablePc: {}
		}
		calcTable[k] = {
			heightList: [],
			monthList: []
		}
	}
	
	sportGroup[k].push(x);
	groupStats[k].cnt += 1;
	
	groupStats[k].monthMin = Math.min(groupStats[k].monthMin, x.month);
	groupStats[k].monthMax = Math.max(groupStats[k].monthMax, x.month);
	calcTable[k].monthList.push( x.month );

	if( !groupStats[k].monthTableCnt[x.month] ){
		groupStats[k].monthTableCnt[x.month] = 0;
	}
	groupStats[k].monthTableCnt[x.month] += 1;
	
});

console.log('---------');
console.log( sportGroup['skiing'][0] );
console.log('---------');
/*
  not much data here
  'speed skating': { label: 'speed skating', cnt: 4 },
  biathlon: { label: 'biathlon', cnt: 6 },
  skiing: { label: 'skiing', cnt: 4 }
*/
var groupStatsList = Object.keys(groupStats).map(function(k){
	return groupStats[k];
}).sort(function(a,b){
	return Number(a.cnt) - Number(b.cnt); //ASC
});

Object.keys(calcTable).forEach(function(k){
	
	//var monthListSorted = calcTable[k].monthList.sort((a,b) => a-b); // ASC
	
	var monthMin = groupStats[k].monthMin;
	var monthMax = groupStats[k].monthMax;
	var len = calcTable[k].monthList.length;
	
	for(var i=monthMin; i<=monthMax; i++){
		var num = groupStats[k].monthTableCnt[i] || 0;
		
		groupStats[k].monthTablePc[i] = Math.round( num / (len * 2) * 1000 ) / 10;
		groupStats[k].monthTablePc[i+12] = Math.round( num / (len * 2) * 1000 ) / 10;
	}
	
	if( ['basketball', 'athletics', 'skiing' ].includes(k) ){
		/*
		month - athletics = 1 / 12
		month - basketball = 1 / 12
		month - skiing = 1 / 10
		*/
		console.log('***');
		console.log('monthMinMax - %s = %s / %s', k, monthMin, monthMax );
		console.log('monthCnt - %s = %s', k, JSON.stringify(groupStats[k].monthTableCnt) );
		console.log('monthPc - %s = %s', k, JSON.stringify(groupStats[k].monthTablePc) );
		
		// TODO try to increase Data for other sports?
	}
	
	// Clustering ( 3 month )
	var monthKeys = Object.keys(groupStats[k].monthTablePc).map(x => Number(x) ).sort((a,b) => a-b); // ASC
	var maxDensityVal = 0;
	var maxDensityKey = null;
	
	monthKeys.forEach(function(m){
		
		// prevent overflow
		if( !groupStats[k].monthTablePc[m+1] || !groupStats[k].monthTablePc[m+2] ){
			return false;
		}
		
		var batchKey = [m, m+1, m+2].join(':');
		/*
		for(var y=0; y<2; y++){
			batchKey += ( Number(m) ).toString();
		}
		*/
		
		var batchSum = groupStats[k].monthTablePc[m] + groupStats[k].monthTablePc[m+1] + groupStats[k].monthTablePc[m+2];
		
		if( batchSum > maxDensityVal ){
			maxDensityVal = batchSum;
			maxDensityKey = batchKey;
		}
	});
	
	var densityPoint = { k: maxDensityKey, p: Math.round(maxDensityVal * 10)/10 };
	groupStats[k].densityPoint = densityPoint;
	
	if( ['basketball', 'athletics' ].includes(k) ){
		console.log('---------');
		console.log('density - %s = %s ', k, JSON.stringify(densityPoint) );
	}
	
	// Calculate Profile Diff - ie how far density from Profile.month
	// Order and exclude 10%
	
});

fs.writeFileSync( REPORT_NAME_DOB, JSON.stringify({ ts: ts, rows: groupStatsList }, null, 2) );
//fs.writeFileSync( REPORT_NAME_MONTH_DISTRO, JSON.stringify({ ts: ts, rows: calcTable }, null, 2) );


return process.exit(0);




