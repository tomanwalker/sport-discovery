
// ## dep
var csv = require('../../lib/csv');
var toolbox = require('../../lib/toolbox');
var fs = require('fs');

// ## conf
var below = 0.333;
var above = 0.667;

// ## fund

// ## flow

var data = csv.readFile('../olympics-modern.csv');
var fil = data.map(function(x){
	
	x.height = Number(x.height);
	x.bmi = Number(x.bmi);
	return x;
	
}).filter(function(x){
	return (
		x.height && x.height > 10
		&& x.bmi && x.bmi > 10
	);
});

console.log('filtered = %s / %s', fil.length, data.length);
var groupBySport = toolbox.analytics.group(fil, ['sport', 'sex'], false);
console.log('---complex---');
console.log(Object.keys(groupBySport));
console.log(groupBySport.groupStats);
console.log('------');

var stats = {m:[], f:[]};

Object.keys(groupBySport.calcTable).forEach(function(k){
	
	var sum = groupBySport.calcTable[k].bmi.reduce(function(a,b){
		return a+b;
	});
	var cnt = groupBySport.groupStats[k].cnt;
	var avg = Math.round(sum / cnt * 100) / 100;
	
	var keyToPush = 'm';
	if( k.includes('female') ){
		keyToPush = 'f';
	}
	stats[keyToPush].push({
		label: k,
		sport: k.split(':')[0],
		cnt: cnt,
		avg: avg
	});
});

var sortByAvg = function(a,b){
	return a.avg - b.avg;
};

stats.m.sort(sortByAvg);
stats.f.sort(sortByAvg);

console.log('---stats---');
console.log(stats);
console.log('------');

fs.writeFileSync('../reports/bmi.json', JSON.stringify(stats, null, 2));



