
// ## dependencies
var common = require('./lib/data');

// ## config

// ## func


// ## flow
console.log('migration');

var file = 'data/all-rio-2016-athletes-excel.csv';
var data = common.readFile(file, ';');

/*
{"name":"A Jesus Garcia","sport":"athletics","nationality":"ESP","sex":"male","date_of_bi
rth":"17/10/1969","age (at start of games)":"46,8","height (m)":"1,72","weight (kg)":"64","BMI":"21,6","gold":"0","sil
ver":"0","bronze":"0","":""}
*/

var mapped = data.map(function(x){
	
	x.month = Number(x['date_of_birth'].split('/')[1]);
	x.height = Math.round(Number(x['height (m)'].replace(',', '.')) * 100);
	x.bmi = Number(x['BMI'].replace(',', '.'));
	x.medal_number = Number(x.gold) * 100 + Number(x.silver) * 10 + Number(x.bronze);
	x.winner = (x.medals > 0);
	
	return {
		name: x.name,
		sport: x.sport,
		country: x.nationality,
		sex: x.sex,
		date_of_birth: x.date_of_birth,
		year: 2016,
		age_there: Number(x["age (at start of games)"].replace(',', '.')),
		month_of_birth: x.month,
		height: x.height,
		weight: Number(x["weight (kg)"].replace(',', '.')),
		bmi: x.bmi,
		gold: Number(x.gold),
		silver: Number(x.silver),
		bronze: Number(x.bronze),
		medal_number: x.medal_number,
		winner: x.winner
	};
});

common.writeFile('data/unified-db.csv', mapped);




