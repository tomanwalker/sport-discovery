
// ## dependencies
var common = require('../lib/data');


// ## flow
console.log('migration 120');

var file = 'original/120-history.csv';
var data = common.readFile(file, ',');

/*
{"ID":"1","Name":"A Dijiang","Sex":"M","Age":"24","Height":"180","Weight":"80","Team":"Ch
ina","NOC":"CHN","Games":"1992 Summer","Year":"1992","Season":"Summer","City":"Barcelona","Sport":"Basketball","Event"
:"Basketball Men's Basketball","Medal":"NA"}
*/

data = data.filter(function(x){
	return Number(x.Year) >= 1970;
});
var mapped = data.map(function(x){
	
	x.height = Math.round(Number(x['Height']));
	x.weight = Number(x['Weight'])
	x.bmi = Math.round( x.weight / Math.pow(x.height, 2) * 10) / 10; //Kg/M2
	
	
	//x.medal_number = 
	//x.winner = (x.medals > 0);
	
	var obj = {
		name: x.Name,
		sport: x.Sport,
		country: x.NOC,
		sex: (x.Sex === 'M') ? 'male' : 'female',
		date_of_birth: null,
		year: Number(x.Year),
		age_there: Number(x["Age"]),
		month_of_birth: 0,
		height: x.height,
		weight: x.weight,
		bmi: x.bmi,
		gold: (x.Medal === 'Gold') ? 1 : 0,
		silver: (x.Medal === 'Silver') ? 1 : 0,
		bronze: (x.Medal === 'Bronze') ? 1 : 0
	};
	
	obj.medal_number = Number(obj.gold) * 100 + Number(obj.silver) * 10 + Number(obj.bronze);
	obj.winner = obj.medals > 0;
	
	return obj;
});

console.log('sample = %j', mapped[0]); // need to group for medal count

var groupped = {};
mapped.forEach( a => {
	
	var id = a.name + a.sport + a.country + a.sex + a.year;
	if( !groupped[id] ){
		groupped[id] = [];
	}
	groupped[id].push(a);
});

var reduced = [];
Object.entries(groupped).forEach(function(tuple){
	
	var x = tuple[1];
	var result = x.reduce(function(r, s){
		
		r.gold += s.gold;
		r.silver += s.silver;
		r.bronze += s.bronze;
		r.medal_number += s.medal_number;
		r.winner = r.medal_number > 0;
		
		return r;
	});
	
	reduced.push(result);
});

console.log("reduced = %j", reduced.find(x => x.bronze > 1));

var file = 'unified-db.csv';
var existing = common.readFile(file, ';');

existing.map(function(a){
	a.id = a.name + a.sport + a.country + a.sex + a.year;
	return a;
});
console.log("DEBUG 1");
reduced.forEach(function(a){
	var id = a.name + a.sport + a.country + a.sex + a.year;
	
	var found = existing.find( x => x.id === id );
	
	if( !found ){
		return existing.push(a);
	}
	
	if( found.height === 0 ){
		found.height = a.height;
	}
	if( found.weight === 0 ){
		found.weight = a.weight;
	}
	if( found.bmi === 0 ){
		found.bmi = Math.round( found.weight / Math.pow(found.height, 2) * 10) / 10;
	}
	
	return true;
	
});
console.log("DEBUG 2");
existing.map(function(a){
	delete a.id;
	return a;
});
console.log("DEBUG 3");
common.writeFile(file, existing);





