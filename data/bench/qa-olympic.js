
// ##
var csv = require('../../lib/csv');
var utils = require('../utils');

// ##
var YEAR_LIMIT = 1980;

var origAll = csv.readFile('../original/olympics-120-history.csv', ',');
var targetData = csv.readFile('../olympics-modern.csv');

// DOB is missing from historical sets
// lets check H and W at least
var origAllFil = origAll.filter(function(x){
	
	return (
		YEAR_LIMIT < Number(x.Year)
		&& x.Height
		&& x.Height !== 'NA'
		&& x.Weight
		&& x.Weight !== 'NA'
		&& !isNaN(x.Weight)
	);
});

var origMapped = origAllFil.map(function(x){
	
	var obj = {
		name: utils.nameShort(x.Name),
		full: x.Name,
		country: x.NOC,
		sport: x.Sport.toLowerCase(),
		year: Number(x.Year),
		height: Number(x.Height),
		weight: Number(x.Weight)
	};
	
	return obj;
});

console.log('filtered sample =', origMapped[5]);
console.log('filtered len =', origAllFil.length);

var missHeight = targetData.filter(function(x){
	return Number(x.height) < 10;
});
console.log('missHeight len =', missHeight.length);
console.log('missHeight sample =', missHeight[5]);

var fixed = [];
missHeight.forEach(function(m){
	origMapped.forEach(function(z){
		if( z.name === m.name && z.sport === m.sport ){
			m.height = z.height;
			m.weight = z.weight;
			m.bmi = utils.calculateBody(m.height, m.weight);
			
			fixed.push(m);
		}
	});
});
console.log('fixed len =', fixed.length);
console.log('fixed sample =', fixed[5]);


//csv.writeFile('../olympics-modern.csv', targetData);

