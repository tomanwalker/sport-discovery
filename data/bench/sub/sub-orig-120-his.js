
// ## dependencies
var csv = require('../../../lib/csv');
var utils = require('../../utils');

// ## config
var subMap = {
	"Athletics.*100 metres": 'sprint',
	"Athletics.*200 metres": 'sprint',
	"Athletics.*400 metres": 'sprint',
	"Athletics.*800 metres": 'sprint',
	"Athletics.*1,500 metres": 'middle',
	"Athletics.*3,000 metres": 'middle',
	"Athletics.*5,000 metres": 'distance',
	"Athletics.*10,000 metres": 'distance',
	"Athletics.*Marathon": 'marathon',
	"Athletics.*Walk": 'walk',
	"Athletics.*Jump": 'jump',
	"Athletics.*Pole": 'jump',
	"Athletics.*Throw": 'throw',
	"Athletics.*Put": 'throw',
	"Athletics.*Steeplechase": 'hurdling',
	"Athletics.*Hurdles": 'hurdling',
	"Athletics.*Pentathlon": 'all around',
	"Athletics.*Heptathlon": 'all around',
	"Athletics.*Decathlon": 'all around',
	
	"Gymnastics.*All-Around": 'artistic',
	"Gymnastics.*Exercise": 'artistic',
	"Gymnastics.*Horse.*": 'artistic',
	"Gymnastics.*Bar": 'artistic',
	"Gymnastics.*Rings": 'artistic',
	"Gymnastics.*Balance Beam": 'artistic',
	"Rhythmic Gymnastics.*": 'rhythmic',
	"Trampolining.*": 'trampolin',
	
	"Water Polo.*": 'water polo',
	"Synchronized Swimming.*": 'artistic',
	"^Swimming.*": 'swimming',
	"Diving": 'diving',
	
	"Cycling.*Sprint": 'track',
	"Cycling.*500 metres Time Trial": 'track',
	"Cycling.*1,000 metres Time Trial": 'track',
	"Cycling.*Pursuit": 'track',
	"Cycling.*Keirin": 'track',
	"Cycling.*Points Race": 'track',
	"Cycling.*Omnium": 'track',
	"Cycling.*Madison": 'track',
	"Cycling.*Road.*": 'road',
	"Cycling.*Individual Time Trial": 'road',
	"Cycling.*100 kilometres Team Time Trial": 'road',
	"Cycling.*Mountainbike.*": 'mountain',
	"Cycling.*BMX": 'bmx',
	
	"Fencing.*Foil": 'foil',
	"Fencing.*epee.*": 'epee',
	"Fencing.*Sabre.*": 'sabre',
	
	'Figure Skating Mixed Ice Dancing': 'dance',
	"Figure Skating Mixed Pairs": "pair",
	"Figure Skating.*Singles": 'single',
	'Figure Skating Mixed Team': 'team',
	
	"Snowboarding.*Slalom": 'slalom',
	"Snowboarding.*Slopestyle": 'slope',
	"Snowboarding.*Boardercross": 'cross',
	"Snowboarding.*Halfpipe": 'halfpipe',
	
	"Light-Heavyweight": 'lightheavyweight',
	"Bantamweight": 'bantamweight',
	"Middleweight": 'middleweight',
	"Middle-Heavyweight": 'middleheavyweight',
	"Middleweight": 'middleweight',
	"Heavyweight": 'heavyweight',
	"Super-Heavyweight": 'superheavyweight',
	"Featherweight": 'featherweight',
	"Lightweight": 'lightweight',
	"Light-Welterweight": 'lightwelterweight',
	"Light-Flyweight": 'lightflyweight',
	"Flyweight": 'flyweight',
	"Welterweight": "welterweight",
	
	"Judo Men's Open Class": 'open',
	
	"Equestrianism.*Jumping.*": 'jump',
	"Equestrianism.*Dressage.*": 'dressage',
	"Equestrianism Mixed Three-Day.*": 'combined',
	
	"Shooting.*Skeet": 'shotgun',
	"Shooting.*Rifle.*": 'rifle',
	"Shooting.*Pistol.*": 'pistol',
	"Shooting.*Trap": 'shotgun',
	"Shooting.*Running Target": 'rifle',
	
	"Cross Country Skiing": 'cross',
	"Alpine Skiing": 'alpine',
	"Freestyle Skiing": 'freestyle',
	
	"Speed Skating.*500 metres": 'longtrack',
	"Speed Skating.*1,000 metres": 'longtrack',
	"Speed Skating.*1,500 metres": 'longtrack',
	"Speed Skating.*3,000 metres": 'longtrack',
	"Speed Skating.*5,000 metres": 'longtrack',
	"Speed Skating.*10,000 metres": 'longtrack',
	"Speed Skating.*Team Pursuit": 'longtrack',
	"Short Track Speed Skating.*": 'shorttrack'
	
};

var skipList = ['Volleyball', 'Handball', 'Football', 'Basketball', 'Table Tennis',
	'Rowing', 'Hockey', 'Baseball', 'Nordic Combined', 'Badminton', 'Curling', 'Biathlon',
	'Ice Hockey', 'Golf', 'Tennis', 'Archery', 'Triathlon', 'Modern Pentathlon',
	'Softball', 'Rugby Sevens', 'Ski Jumping', 'Beach Volleyball',
	'Canoeing', 'Bobsleigh', 'Luge', 'Sailing'];

var skipListLow = skipList.map( w => w.toLowerCase() );

// ## func
var origMapper = function(x){
	
	return {
		name: utils.nameShort(x.Name),
		full: x.Name,
		sport: x.Sport,
		country: x.NOC,
		year: Number(x.Year),
		eve: x.Event
	};
};
var dataMapper = function(x){
	
	x.year = Number(x.year);
	return x;
};

var origFilter = function(x){
	return (
		Number(x.year) >= 1980
		&& !skipList.includes( x.sport )
		&& x.sport === 'Synchronized Swimming'
	);
};

var yearSorter = function(a,b){
	return (
		Number(a.year) - Number(b.year)
	)
};

// ## flow

var subCsv = csv.readFile('../../original/olympics-120-history.csv', ',');
var subSource = subCsv.map(origMapper).filter(origFilter).sort(yearSorter);

var data = csv.readFile('../../olympics-modern.csv');
var fil = data.filter(function(x){
	return (
		//x.sub === '' 
		x.sport === 'aquatics'
		&& !skipListLow.includes( x.sport )
	);
	
}).sort(yearSorter);

console.log('------------');
console.log('mapped.sample = %j', subSource[12]);
console.log('------------');
console.log('fil.sample = %j', fil[12]);
console.log('------------');

var cntFound = 0;
var runLimit = 7;
var runCnt = 0;

//var yearStrart = 1980;
var yearStrart = 1980;
var yearEnd = 2020;

for(var y=yearStrart; y<yearEnd; y++){
	
	if( y % 5 === 0 ){
		console.log('loop.year = %s', y);
	}
	
	fil.filter(x => Number(x.year) === y ).forEach(function(d){
		subSource.filter(x => Number(x.year) === y ).forEach(function(s){
			
			if( d.name === s.name && d.full === s.full && Number(d.year) === Number(s.year) && s.country === s.country ){
				
				var mapList = Object.keys(subMap);
				var mapKey = s.eve;
				
				var found = mapList.find( x => mapKey.match( new RegExp(x) ) );
				
				if( found ){
				//if( false ){
					d.sub = subMap[found];
					cntFound += 1;
				}
				else {
					console.log('found = %s / %s / %s / %s', d.name, d.sport, s.sport, s.eve);
					runCnt += 1;
					
					if( runCnt > runLimit ){
						
						console.log('------------');
						console.log('found = %s / data = %s', cntFound, fil.length);
						return process.exit();
					}
				}
			}
		});
	});
	
}

console.log('------------');
console.log('found = %s / source = %s / data = %s', cntFound, subSource.length, fil.length);

//csv.writeFile('../olympics-modern.csv', data);
csv.writeFile('test.csv', data);








