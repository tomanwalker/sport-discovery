
// ## dependencies
var csv = require('../../lib/csv');

// ## config

// EQ =  Dressage, Eventing, and Jumping.

var subMap = {
	'aquatics-swimming': 'swimming',
	'aquatics-water polo': 'waterpolo',
	'equestrian-jumping': 'jumping',
	'gymnastics-rhythmic g.': 'rhythmic',
	'aquatics-synchronized s.': 'artistic',
	'cycling-cycling track': 'track',
	'equestrian-eventing': 'eventing',
	'aquatics-diving': 'diving',
	'gymnastics-artistic g.': 'artistic',
	'cycling-cycling road': 'road',
	'cycling-mountain bike': 'mountain',
	'equestrian-dressage': 'dressage',
	'cycling-bmx': 'bmx',
	'gymnastics-trampoline': 'trampoline'
};
var skipList = ['wrestling free.', 'wrestling gre-r', 'beach volley.'];

// ## func
function capitalizeFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}
var summerMapper = function(x){
	/*
	 {"City":"Montreal","Year":"1976","Sport":"Aquatics","Discipline":"Diving","Event":"3m springboard","Athlete"
	:"K�HLER, Christa","Gender":"Women","Country_Code":"GDR","Country":"East Germany","Event_gender":"W","Medal":"Silver"}
	*/
	
	var sp = x.Athlete.split(', ');
	var nameUnicode = sp[1] + ' ' + capitalizeFirstLetter(sp[0].toLowerCase());
	var name = nameUnicode;
	
	var sex = ( x.Gender === 'Women' ) ? 'female' : 'male';
	
	return {
		name: name,
		year: x.Year,
		sport: x.Sport.toLowerCase(),
		dis: x.Discipline.toLowerCase(),
		sex: sex,
		country: x.Country_Code
	};
};
var summerFilter = function(s){
	return (
		Number(s.year) >= 1980
		&& !skipList.includes( s.dis )
	);
};

// ## flow

var data = csv.readFile('../olympics-modern.csv');

var subCsv = csv.readFile('../original/Summer-Olympic-medals-1976-to-2008.csv', ',');
var subSource = subCsv.map(summerMapper).filter(summerFilter);

var fil = data.filter( x => x.sub === '' );

console.log('------------');
console.log('mapped.sample = %j', subSource[0]);
console.log('------------');
console.log('fil.sample = %j', fil[0]);
console.log('------------');

var cntFound = 0;

fil.forEach(function(d){
	subSource.forEach(function(s){
		if( d.name === s.name && d.sport === s.sport && s.sport !== s.dis ){
			
			var mapKey = s.sport + '-' + s.dis;
			
			if( subMap[mapKey] ){
				d.sub = subMap[mapKey];
				cntFound += 1;
			}
			else {
				console.log('found = %s / %s / %s', d.name, d.sport, s.dis);
				return process.exit();
			}
		}
	});
});

console.log('------------');
console.log('found = %s / data = %s', cntFound, data.length);

csv.writeFile('../olympics-modern.csv', data);





