
// ## dependencies
var csv = require('../../lib/csv');

// ## config
var noneList = [
	"badminton", "biathlon", "table tennis", "golf",
	"nordic combined", "ski jumping", "triathlon",
	"archery", "tennis"
];

// ## flow
var data = csv.readFile('../olympics-modern.csv');

data.forEach(function(x){
	
	if( noneList.includes( x.sport ) && x.sub === '' ){
		x.sub = 'none'
	}
	
});

csv.writeFile('test.csv', data);




