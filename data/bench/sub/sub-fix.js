
// ## dependencies
var csv = require('../../lib/csv');

// ## config
var badList = {
	"field hockey": ["lightweight", "swimming", "sprint", "heavyweight", "welterweight"],
	"athletics": ["water polo", "longtrack", "heavyweight", "track"],
	"fencing": ["jump","walk","diving","track"],
	"gymnastics": ["sprint","longtrack"],
	"aquatics": ["rifle", "lightheavyweight","throw"],
	"shooting": ["lightweight"],
	
	
	"weightlifting": ["swimming", "diving", "alpine", "pistol"],
	"wrestling": ["jump"],
	"cycling": ["longtrack","heavyweight","swimming", "cross", "walk", "water polo"],
	"boxing": ["shotgun"],
	"speed skating": ["track", "rifle", "none", ],
	"skiing": ["track", "rhythmic", "road", "slalom"]
};
var replaceList = {
	"athletics": {a: "relay", b: "sprint"},
	"gymnastics": {a: "artistic ", b: "artistic"},
	
	"aquatics": {a: "sprint", b: "swimming" },
	
	"equestrian": {a: "jumping", b: 'jump' },
	"cycling": {a: "road,track", b: "road" },
	"boxing": {a: "heavyweight,", b: "heavyweight" },
	"skiing": {a: "crosscountry", b: "cross"}
};

// ## flow
var data = csv.readFile('../olympics-modern.csv');

data.forEach(function(x){
	
	if( badList[ x.sport ] && badList[ x.sport ].includes( x.sub ) ){
		x.sub = '';
	}
	
	if( replaceList[ x.sport ] && replaceList[ x.sport ].a === x.sub ){
		x.sub = replaceList[ x.sport ].b;
	}
	
});

csv.writeFile('../olympics-modern.csv', data);




