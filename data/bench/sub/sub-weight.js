
// ## dependencies
var csv = require('../../lib/csv');

// Map is subject to change over time
// Current constructed at 20201107
var wMap = {
	"taekwondo": {
		'male': [{
			label: 'flyweight',
			min: 20,
			max: 58
		},{
			label: 'featherweight',
			min: 58,
			max: 68
		},{
			label: 'welterweight',
			min: 68,
			max: 80
		},{
			label: 'heavyweight',
			min: 80,
			max: 200
		}],
		female: [{
			label: 'flyweight',
			min: 20,
			max: 49
		},{
			label: 'featherweight',
			min: 49,
			max: 57
		},{
			label: 'welterweight',
			min: 57,
			max: 67
		},{
			label: 'heavyweight',
			min: 67,
			max: 120
		}]
	},
	"judo": {
		'male':[{
			label: 'extralightweight',
			min: 20,
			max: 60
		},{
			label: 'halflightweight',
			min: 60,
			max: 66
		},{
			label: 'lightweight',
			min: 66,
			max: 73
		},{
			label: 'halfmiddleweight',
			min: 73,
			max: 81
		},{
			label: 'middleweight',
			min: 81,
			max: 90
		},{
			label: 'halfheavyweight',
			min: 90,
			max: 100
		},{
			label: 'heavyweight',
			min: 100,
			max: 300
		}],
		'female':[{
			label: 'extralightweight',
			min: 10, max: 47
		},{
			label: 'halflightweight',
			min: 47,
			max: 52
		},{
			label: 'lightweight',
			min: 52, max: 57
		},{
			label: 'halfmiddleweight',
			min: 57, max: 63
		},{
			label: 'middleweight',
			min: 63, max: 70
		},{
			label: 'halfheavyweight',
			min: 70,
			max: 78
		},{
			label: 'heavyweight',
			min: 78, max: 200
		}]
	},
	"boxing": {
		male: [{
			label: 'lightflyweight',
			min: 20,
			max: 49
		},{
			label: 'flyweight', min: 49, max: 52 
		},{
			label: 'bantamweight', min: 52, max: 57
		},{
			label: 'lightweight', min: 57, max: 60
		},{
			label: 'lightwelterweight', min: 60, max: 64
		},{
			label: 'middleweight', min: 69, max: 75
		},{
			label: 'heavyweight', min: 81, max: 91 
		},{
			label: 'superheavyweight', min: 90, max: 300
		}]
	},
	"weightlifting": {
		male: [{
			label: 'bantamweight', min: 10, max: 55 
		},{
			label: 'featherweight', min: 55, max: 62
		},{
			label: 'lightweight', min: 62, max: 69
		},{
			label: 'middleweight', min: 74, max: 77
		},{
			label: 'lightheavyweight', min: 77, max: 85
		},{
			label: 'middleheavyweight', min: 85, max:94 
		},{
			label: 'heavyweight', min: 94, max: 105
		},{
			label: 'superheavyweight', min: 105, max: 200
		}],
		female: [{
			label: 'flyweight', min: 20, max: 49
		},{
			label: 'featherweight', min: 49, max: 53
		},{
			label: 'lightweight', min: 53, max: 58
		},{
			label: 'middleweight', min: 58, max: 63
		},{
			label: 'lightheavyweight', min: 63, max: 69
		},{
			label: 'heavyweight', min: 69, max: 75
		},{
			label: 'superheavyweight', min: 75, max: 200
		}]
	},
	"wrestling": {
		'male': [{
			label: 'welterweight',
			min: 62,
			max: 65
		},{
			label: 'lightheavyweight',
			min: 83,
			max: 86
		},{
			label: 'superheavyweight',
			min: 122,
			max: 125
		}],
		'female':[{
			label: 'middleweight',
			min: 60,
			max: 63
		}]
	}
};
var wList = Object.keys(wMap);


var data = csv.readFile('../olympics-modern.csv');
console.log('----------');
var cnt = 0;

data.forEach(function(x){
	
	var wNum = Number(x.weight);
	
	if( wNum > 0 && x.sub === '' && wList.includes( x.sport ) ){
		
		var defined = wMap[x.sport][x.sex] || [];
		var found = defined.find( c => wNum > c.min && wNum <= c.max );
		
		if( found ){
			x.sub = found.label;
			cnt += 1;
		}
		else {
			//console.log( x );
			//console.log( '====' );
			//console.log( cnt );
			//return process.exit(0);
		}
	}
	
});

console.log( '==done==' );
console.log( cnt );
//return process.exit(0);
csv.writeFile('../olympics-modern.csv', data);






