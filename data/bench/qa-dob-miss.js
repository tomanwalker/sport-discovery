
// ## dependencies
var rp = require('request-promise');

var csv = require('../../lib/csv');
var utils = require('../utils');

// ## conf
var BASE_URL = 'https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=';
var LOOP_START = 0;
var LOOP_END = 5;

// ## func
var mapper = function(x){
	
	x.height = Number(x.height);
	x.weight = Number(x.weight);
	x.medal_number = Number(x.medal_number);
	x.year = Number(x.year);
	
	return x;
};
var dobFilter = function(x){
	return (
		x.height > 10
		&& x.weight > 10
		&& x.date_of_birth.length === 0
	);
};
var sorter = function(a, b){
	
	var diff = b.medal_number - a.medal_number;
	if( diff !== 0 ){
		return diff;
	}
	
	return b.year - a.year;
};

var nameSorter = function(a, b){
	if(a.name < b.name){
		return -1;
	}
	if(a.name > b.name){
		return 1;
	}
	if(a.country < b.country){
		return -1;
	}
	if(a.country > b.country){
		return 1;
	}
	
	return a.year - b.year;
};

var run = async function(){
	
	var LOOP_START = 50;
	var LOOP_END = 500;
	
	var cnt = 0;
	var range = LOOP_END - LOOP_START;
	var start = new Date();
	var sel = mapped.slice(LOOP_START, LOOP_END);
	
	for(const x of sel){
		
		var url = BASE_URL + x.name.replace(' ', '%20');
		try{
			var result = await rp(url);
			
			/*
			https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=Stack%20Overflow
			
			https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=Jodie%20Henry
			
			should contain = Olympic, born (born 17 November 1983)
			*/
			//console.log(result);
			/*
			{
				"batchcomplete":"",
				"query":{
					"pages":{
						"901544":{
							"pageid": 901544,
							"ns": 0,
							"title":"Jodie Henry",
							"extract":"Jodie Clare Henry, OAM (born17 November 1983) is an Australian competitive swimmer, Olympic gold medallist and former world-record holder."
						}
					}
				}
			}
			*/
			
			var el = JSON.parse(result).query.pages;
			var pageKeys = Object.keys(el);
			
			if( pageKeys[0] !== '-1' ){
				var txt = el[pageKeys[0]].extract;
				
				var reqText = (
					( txt.includes('Olympic') || txt.includes('olympic') )
					&& txt.includes('born')
				);
				
				if( reqText ){
					
					var found = txt.match(/born[\s\,\w\d]{3,20}\d{4}/);
					if( found && found[0] ){
						
						var here = found[0].replace('born ', "");
						//console.log(here);
						
						try{
							var d = new Date(here);
							var dob = utils.dateformat(d);
							
							//console.log(dob);
							x.date_of_birth = dob;
							cnt += 1;
							
							var fil = data.filter(function(s){
								return (
									x.full === s.full
									&& x.sport === s.sport
									&& x.country === s.country
									&& x.year !== s.year
								);
							});
							fil.forEach(function(s){
								s.date_of_birth = dob;
							});
							
							if( cnt % Math.round((range)/10) === 0 ){
								console.log(cnt);
							}
							
						}
						catch(e){}
					}
				}
			}
			
		}
		catch(e){}
		
	}
	
	var end = new Date();
	var diff = end - start;
	
	console.log('-----');
	console.log('Done - cnt = %s | range = %s | st = %s | en = %s', 
		cnt, range, start.toISOString(), end.toISOString());
	csv.writeFile('test.csv', data.sort(nameSorter));
};

// ## flow
var data = csv.readFile('../olympics-modern.csv');
var mapped = data.map(mapper).filter(dobFilter).sort(sorter);
var pc = Math.round(mapped.length / data.length * 1000) / 10;

console.log('----');

//start - 96294 / 113390 / 84.9 %
console.log('start - %s / %s / %s %', mapped.length, data.length, pc);
console.log('sample - %s / %s', mapped[0].name, mapped[0].sport);

run();



