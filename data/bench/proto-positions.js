

// ## dependencies
var fs = require('fs');
var csv = require('../../lib/csv');
var toolbox = require('../../lib/toolbox');

// ## config
var profile = {
	h: 186,
	g: 'male'
};

var REPORT_NAME_SUB = '../reports/sub-presense.json';
var MISS_LABEL = 'miss';

var time = new Date();
var ts = time.toISOString();

// ## func
var randomBetween = function(min, max) { 
	// min and max included 
	return Math.floor(Math.random() * (max - min + 1) + min);
};

// ## flow
var data1 = csv.readFile('../olympics-modern.csv');
var data2 = csv.readFile('../nba.csv');

var total = data1.length + data2.length;
var combined = data1.concat( data2 );

console.log('============');
console.log('mapped.len = %s / %s / %s %', combined.length, total, Math.round(combined.length / total * 100) );
console.log('mapped.sample = %j', combined[randomBetween(0, combined.length)]);
console.log('============');

var present = 0;
var subMap = {};

combined.forEach(function(x){
	
	if( !subMap[x.sport] ){
		subMap[x.sport] = {
			sport: x.sport,
			cnt: 0,
			pr: 0,
			pr_pc: 0,
			subs: {}
		};
	}
	subMap[x.sport].cnt += 1;
	if( x.sub !== '' ){
		present += 1;
		subMap[x.sport].pr += 1;
		subMap[x.sport].pr_pc = Math.round( subMap[x.sport].pr / subMap[x.sport].cnt * 10000 ) / 100;
	}
	
	var sp = x.sub.split('/');
	sp.forEach(function(z){
		
		var subKey = z || 'miss';
		
		if( !subMap[x.sport].subs[subKey] ){
			subMap[x.sport].subs[subKey] = {
				cnt: 0,
				pc: 0
			};
			
			if( 'basketball' === x.sport ){
				subMap[x.sport].subs[subKey].h_min = 300;
				subMap[x.sport].subs[subKey].h_sum = 0;
			}
		}
		
		subMap[x.sport].subs[subKey].cnt += 1;
		subMap[x.sport].subs[subKey].pc = Math.round(subMap[x.sport].subs[subKey].cnt / subMap[x.sport].cnt * 10000) / 100;
		
		if( 'basketball' === x.sport ){
			var h = Number(x.height);
			if( h > 10 ){
				subMap[x.sport].subs[subKey].h_min = Math.min(subMap[x.sport].subs[subKey].h_min, h);
				subMap[x.sport].subs[subKey].h_sum += h;
				subMap[x.sport].subs[subKey].h_avg = Math.round(subMap[x.sport].subs[subKey].h_sum / subMap[x.sport].subs[subKey].cnt * 10) / 10;
			}
		}
		
	});
});

var subList = [];
Object.keys(subMap).forEach(function(k){
	subList.push( subMap[k] );
});

subList = subList.sort(function(a,b){
	return a.pr_pc - b.pr_pc; //ASC
});

var report = {
	ts: ts,
	present: present,
	total: total,
	pr_pc: Math.round( present / total * 10000) / 100,
	rows: subList
};


fs.writeFileSync( REPORT_NAME_SUB, JSON.stringify(report, null, 2) );
return process.exit(0);

/*
// ### Groupping
var draftData = mapped.filter( x => x.sex === profile.g );
draftData = draftData.map(function(x){
	
	x.bmi = Number(x.bmi);
	x.height = Number(x.height);
	
	// miss/other/empty sub ==> empty
	if( x.sub === 'miss' ){
		x.sub = ''
	}
	if( x.sub === 'other' ){
		x.sub = ''
	}
	
	// less than 10% ==> empty
	var targetSportSubs = groupStatsList.find( r => r.label === x.sport );
	var subPresenceCnt = targetSportSubs.subs[ x.sub ] || 0;
	var subPresencePc = (subPresenceCnt / targetSportSubs.cnt);
	
	// gt than 10% ==> both common and specific
	if( subPresencePc < 0.05 ){
		x.sub = '';
	}
	
	return x;
	
}).filter(function(x){
	
	return x.height > 10
});

var draftGroupResults = toolbox.analytics.group(draftData, ['sport', 'sub'], {doubleKey: true});
var draftGroups = draftGroupResults.groups;
var draftGroupStats = draftGroupResults.groupStats;
var draftCalcTable = draftGroupResults.calcTable;

var draftGroupKeys = Object.keys(draftGroupStats);
draftGroupKeys.forEach(function(k){
	
	if( k.includes('basketball') ){
		console.log('groupStats = %j', draftGroupStats[k]);
	}
});

console.log('draft - groupping - amount = %s', Object.keys(draftGroups).length);

// evaluate height percentile & diff
for(var k in draftGroups){
	var low = 0.15;
	var mid = 0.50;
	var high = 0.85;
	
	var limit_step = 1;
	var limit_under = (mid*100) - limit_step; // => 47
	var limit_above = (mid*100) + limit_step;
	
	var len = draftCalcTable[k].height.length;
	draftCalcTable[k].height = draftCalcTable[k].height.sort(); // ASC
	
	draftGroupStats[k].h_min = draftCalcTable[k].height[0];
	draftGroupStats[k].h_max = draftCalcTable[k].height[len-1];
	draftGroupStats[k].h_low = Math.round( toolbox.analytics.pct(draftCalcTable[k].height, low, true) );
	draftGroupStats[k].h_mid = toolbox.analytics.round( toolbox.analytics.pct(draftCalcTable[k].height, mid, true), 2);
	draftGroupStats[k].h_high = Math.round( toolbox.analytics.pct(draftCalcTable[k].height, high, true) );
	
	draftGroupStats[k].h_avg = toolbox.analytics.round( (draftCalcTable[k].h_sum / len), 1 );
	draftGroupStats[k].h_avg_diff = toolbox.analytics.round(draftGroupStats[k].h_avg - draftGroupStats[k].h_mid, 1);
	draftGroupStats[k].h_mid = Math.round(draftGroupStats[k].h_mid);
	
	// profile match
	var checkHeight = toolbox.analytics.pctRank(draftCalcTable[k].height, profile.h);
	draftGroupStats[k].h_pct = 0.0;
	
	if( checkHeight > -1){
		draftGroupStats[k].h_pct = toolbox.analytics.round(checkHeight * 100, 1);
		
		// sport Outliers influence profile.pctRank
		draftGroupStats[k].h_pct = toolbox.analytics.round((draftGroupStats[k].h_pct - draftGroupStats[k].h_avg_diff / 2), 1);
	}
	//log.debug('DEBUG - k = %s | c = %s | p = %s', k, checkHeight, groupStats[k].h_pct);
	
	// a bit flexible around percentile
	draftGroupStats[k].h_pct_diff = 0;
	if( draftGroupStats[k].h_pct >= limit_above ){
		draftGroupStats[k].h_pct_diff = toolbox.analytics.round((draftGroupStats[k].h_pct - limit_above), 1);
	}
	if( draftGroupStats[k].h_pct <= limit_under ){
		draftGroupStats[k].h_pct_diff = toolbox.analytics.round((limit_under - draftGroupStats[k].h_pct), 1);
	}
	
	// eval index
	draftGroupStats[k].h_diff_pc = draftGroupStats[k].h_pct_diff / limit_under;
	draftGroupStats[k].index = toolbox.analytics.round((1 - draftGroupStats[k].h_diff_pc) * 100, 1);
	
}

console.log('draft - group stats - sample = %j', groupStats["basketball"]);
console.log('draft - group stats - sample = %j', groupStats["football"]);
console.log('draft - group stats - sample = %j', groupStats["athletics"]);

// look for matches
var draftGroupStatsList = Object.entries(draftGroupStats).map( x => x[1] );

var heightMatch = draftGroupStatsList.filter(function(x){
	return true;
}).map(function(x){
	
	var att = ['key', 'h_pct_diff', 'h_low', 'h_mid', 'h_high', 'h_pct', 'index', 'cnt'];
	var obj = {};
	
	att.forEach(function(k){
		obj[k] = x[k];
	});
	
	return obj;
	
}).sort(function(a, b){
	
	var pct_diff = a.h_pct_diff - b.h_pct_diff; // DESC
	if( pct_diff !== 0 ){
		return pct_diff;
	}
	
	var abs_diff = Math.abs(a.h_mid - profile.height) - Math.abs(b.h_mid - profile.height);
	return abs_diff;
});

console.log('draft - calc - heightMatch = %j', heightMatch.slice(0, 10));

return process.exit(0);

*/


