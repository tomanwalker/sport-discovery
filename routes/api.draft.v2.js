
// ## dependencies
var express = require('express');
var server = express.Router();

var toolbox = require('../lib/toolbox');

// ### export
var ns = {router: server};
module.exports = ns;

// ### config
var log = toolbox.getLogger('routes/api.draft.v2');

// ### func
ns.filterWeight = function(req, res, next){
	
	var profile = req.body;
	log.debug('filterWeight - start - user = %s | profile = %j', 
		res.locals.username, profile);
	
	if( !profile.weight ){
		return next();
	}
	
	var hei = profile.height / 100;
	profile.bmi = Math.round( ( profile.weight / (hei ^ 2) ) * 10) / 10;
	
	// 18,5 to 24,9 is normal
	var W_LOW = 19.5;
	var W_HIGH = 23.9;
	var TO_CUT = 6; //roughly 20% of the report Set
	
	var wType = 0;
	if( profile.bmi < W_LOW ){
		wType = -1;
	}
	if( profile.bmi > W_HIGH ){
		wType = 1;
	}
	
	log.debug('filterWeight - mid - wType = %s', wType);
	
	// load Weight report
	var bmi_stats = toolbox.dal.reports.bmi();
	if( profile.gender === 'male' ){
		bmi_stats = bmi_stats.m;
	}
	else {
		bmi_stats = bmi_stats.f;
	}
	
	bmi_stats.sort(function(a,b){
		return a.avg - b.avg;
	});
	
	log.debug('filterWeight - report - cnt = %s | sample = %j', 
		bmi_stats.length, bmi_stats[0]);
	
	// based on wType exlude N items from loaded data
	var avoid = [];
	
	if( wType === -1 ){
		//slim - cut from end
		avoid = bmi_stats.slice(-TO_CUT);
	}
	else if( wType === 1 ){
		avoid = bmi_stats.slice(0, TO_CUT);
	}
	else {
		var half_cut = TO_CUT / 2;
		avoid = bmi_stats.slice(0, half_cut);
		avoid = avoid.concat(bmi_stats.slice(-half_cut));
	}
	log.debug('filterWeight - cut - avoid = %j', avoid );
	
	var avoidFlat = avoid.map(x => x.sport);
	var dataList = res.locals.data.rows;
	log.debug('filterWeight - before = %s', dataList.length );
	dataList = dataList.filter(function(d){
		return !avoidFlat.includes(d.sport);
	});
	log.debug('filterWeight - after = %s', dataList.length );
	
	res.locals.data.rows = dataList;
	log.debug('filterWeight - end - keys = %j', Object.keys(res.locals.data) );
	return next();
	
};

ns.groupData = function(req, res, next){
	
	var profile = res.locals.profile;
	var dataList = res.locals.data.rows;
	log.debug('groupData - start - user = %s | profile = %j', 
		res.locals.username, profile);
	
	var athleteMap = {};
	var metricList = [];
	
	dataList.forEach(function(d){
		
		var personLabel = [d.full, d.sport, d.country].join('-');
		if( !athleteMap[personLabel] ){
			athleteMap[personLabel] = {
				name: d.name,
				full: d.full,
				sport: d.sport,
				country: d.country,
				height: 0,
				weight: 0,
				bmi: 0,
				total: 0,
				results: [],
				m_arr: [],
				ocur: []
			};
		}
		athleteMap[personLabel].ocur.push( d );
		
		var subText = d.sub.replace('none', 'other');
		if( subText === '' ){
			subText = 'other';
		}
		
		var subList = subText.split('/');
		subList.forEach(function(s){
			var obj = {
				sport: d.sport,
				sub: s,
				date_of_birth: d.date_of_birth,
				height: Number(d.height),
				weight: Number(d.weight),
				bmi: Number(d.bmi),
				medal_number: Number(d.medal_number)
			};
			metricList.push( obj );
		});
		
	});
	
	var groupKey = ["sport"];
	if( profile.division ){
		groupKey.push('sub');
	}
	var grouppingResult = toolbox.analytics.group(metricList, groupKey);
	
	var groupStats = grouppingResult.groupStats;
	var calcTable = grouppingResult.calcTable;
	
	// evaluate height percentile & diff
	for(var k in calcTable){
		var low = 0.15;
		var mid = 0.50;
		var high = 0.85;
		
		var limit_step = 3;
		var limit_under = (mid*100) - limit_step; // => 47
		var limit_above = (mid*100) + limit_step;
		
		var len = calcTable[k].height.length;
		
		calcTable[k].height = calcTable[k].height.sort(); // ASC
		
		groupStats[k].h_min = calcTable[k].height[0];
		groupStats[k].h_max = calcTable[k].height[len-1];
		groupStats[k].h_low = Math.round( toolbox.analytics.pct(calcTable[k].height, low, true) );
		groupStats[k].h_mid = toolbox.analytics.round( toolbox.analytics.pct(calcTable[k].height, mid, true), 2);
		groupStats[k].h_high = Math.round( toolbox.analytics.pct(calcTable[k].height, high, true) );
		
		groupStats[k].h_avg = toolbox.analytics.round( (calcTable[k].h_sum / len), 1 );
		groupStats[k].h_avg_diff = toolbox.analytics.round(groupStats[k].h_avg - groupStats[k].h_mid, 1);
		groupStats[k].h_mid = Math.round(groupStats[k].h_mid);
		
		// profile match
		var checkHeight = toolbox.analytics.pctRank(calcTable[k].height, profile.height);
		groupStats[k].h_pct = 0.0;
		
		if( checkHeight > -1){
			groupStats[k].h_pct = toolbox.analytics.round(checkHeight * 100, 2);
			
			// sport Outliers influence profile.pctRank
			groupStats[k].h_pct_shift = groupStats[k].h_pct;
			if( groupStats[k].h_pct > 0 && groupStats[k].h_pct < 100 ){
				groupStats[k].h_pct_shift = toolbox.analytics.round((groupStats[k].h_pct - groupStats[k].h_avg_diff / 2), 2);
			}
		}
		
		// a bit flexible around percentile
		groupStats[k].h_pct_diff = 0;
		if( groupStats[k].h_pct_shift >= limit_above ){
			groupStats[k].h_pct_diff = toolbox.analytics.round((groupStats[k].h_pct_shift - limit_above), 2);
		}
		if( groupStats[k].h_pct_shift <= limit_under ){
			groupStats[k].h_pct_diff = toolbox.analytics.round((limit_under - groupStats[k].h_pct_shift), 2);
		}
		
		// eval index
		groupStats[k].h_diff_pc = groupStats[k].h_pct_diff / limit_under;
		groupStats[k].index = toolbox.analytics.round((1 - groupStats[k].h_diff_pc) * 100, 1);
		if( groupStats[k].index < 0 ){
			groupStats[k].index = 0;
		}
		
	}
	
	var debugInterestKeys = Object.keys(groupStats).filter(function(x){
		return x.includes('basketball');
	});
	debugInterestKeys.forEach(function(x){
		log.debug('draft - group stats - sample = %j', groupStats[x]);
		//log.debug('draft - group stats - sample = %j', groupStats["football"]);
		//log.debug('draft - group stats - sample = %j', groupStats["athletics"]);
	});
	
	// look for matches
	var groupStatsList = Object.entries(groupStats).map( x => x[1] );
	var heightMatch = groupStatsList.filter(function(x){
		
		// perhaps here - exlude the ones MAX or MIN exceeded?
		return true;
		
	}).sort(function(a, b){
		
		var pct_diff = Math.round(a.h_pct_diff) - Math.round(b.h_pct_diff); // DESC
		if( pct_diff !== 0 ){
			return pct_diff;
		}
		
		var abs_diff = Math.abs(a.h_mid - profile.height) - Math.abs(b.h_mid - profile.height); // ASC
		return abs_diff;
		
	});
	log.debug('draft - calc - heightMatch = %j', heightMatch.slice(0, 10));
	
	res.locals.topSports = heightMatch.slice(0, profile.result_len);
	res.locals.topArr = res.locals.topSports.map(x => x.key);
	res.locals.athleteMap = athleteMap;
	return next();
};

ns.similarAthletes = function(req, res, next){
	
	var profile = res.locals.profile;
	var athleteMap = res.locals.athleteMap;
	var topArr = res.locals.topArr;
	
	var topArrUniqueMap = {};
	topArr.forEach(function(t){
		var sp = t.split(':')[0];
		if( !topArrUniqueMap[sp] ){
			topArrUniqueMap[sp] = 1;
		}
	});
	var topArrUnique = Object.keys(topArrUniqueMap);
	
	var athleteList = [];
	
	log.debug('similarAthletes - start - topArr = %j', topArrUnique);
	
	for(var a in athleteMap){
		
		var obj = athleteMap[a];
		var partialMatch = topArrUnique.find(function(t){
			return t.includes( obj.sport )
		});
		
		if( !partialMatch ){
			continue;
		}
		
		obj.ocur.forEach(function(x){
			
			obj.total += x.medal_number;
			obj.m_arr.push({
				h: x.height,
				w: x.weight,
				b: x.bmi,
				y: ( x.year || x.year_end )
			});
			 
			var rEntry = {
				g: x.gold,
				s: x.silver,
				b: x.bronze
			};
			if( x.year ){
				rEntry.year = x.year;
			}
			else{
				rEntry.ys = x.year_start;
				rEntry.ye = x.year_end;
				rEntry.m = x.mvp;
			}
			
			obj.results.push(rEntry);
		});
		
		athleteList.push(obj)
	}
	
	log.debug('similarAthletes - before filter - list = %s / %j', athleteList.length, athleteList[0]);
	
	athleteList = athleteList.map(function(x){
		
		var lastMetric = x.m_arr.sort(function(a,b){
			return b.y - a.y;
		})[0];
		
		x.height = lastMetric.h;
		x.weight = lastMetric.w;
		x.bmi = lastMetric.b;
		x.last_year = lastMetric.y;
		
		delete x.m_arr;
		delete x.ocur;
		return x;
		
	}).filter(function(x){
		return (
			Math.abs(x.height - profile.height) < 10
		);
	});
	
	/*
	{ 
	  "name":"Aarne Lindroos", "full":"Aarne Ulf Kristian Lindroos",
	  "sport":"rowing","country":"FIN","height":192,"weight":88.5,"bmi":24,
	  "total":0,
	  "results":[{"g":"0","s":"0","b":"0","y":1984},{"g":"0","s":"0","b":"0","y":1988}]
	}
	*/
	log.debug('similarAthletes - after filter - list = %s / %j', athleteList.length, athleteList[0]);
	
	var athleteSamples = [];
	var topArrUse = topArrUnique;
	/*
	if( topArrUse.length > 2 ){
		topArrUse = topArrUse.slice(0, -1);
	}
	*/
	
	topArrUse.forEach(function(s){
		
		log.debug('topArrUse = %s', s);
		
		var sportFound = athleteList.filter( x => x.sport === s );
		if( sportFound.length === 0 ){
			return false;
		}
		
		var athleteFound = sportFound.sort(function(a, b){
			
			// 1cm is not important
			var heightDiffA = Math.floor(Math.abs(a.height - profile.height) / 2); 
			var heightDiffB = Math.floor(Math.abs(b.height - profile.height) / 2);
			var heightDiffSort = heightDiffA - heightDiffB; // ASC
			
			if( heightDiffSort !== 0 ){
				return heightDiffSort;
			}
			
			var medalDiff = b.total - a.total; // DESC
			if( medalDiff !== 0 ){
				return medalDiff;
			}
			
			return b.last_year - a.last_year;  // DESC
			
		})[0];
		
		athleteSamples.push( athleteFound );
		
	});
	
	log.debug('similarAthletes - samples - list = %s / %j', athleteSamples.length, athleteSamples[0]);
	
	res.locals.athleteSamples = athleteSamples;
	return next();
};

ns.saveProfile = function(req, res, next){
	// TODO save to db, profile + results snapshot
	return next();
};

ns.prepareResponse = function(req, res){
	
	var profile = res.locals.profile;
	var start = res.locals.timeStart;
	var topKey = res.locals.topArr[0];
	
	var topSportList = res.locals.topSports.map(function(x){
		var att = ['key', 'index', 'h_low', 'h_mid', 'h_high'];
		var obj = {};
		
		att.forEach(function(k){
			obj[k] = x[k];
		});
		
		return obj;
	});
	var roster = res.locals.athleteSamples.map(function(x){
		
		var att = ['name', 'sport', 'country', 'height', 'weight', 'bmi', 
			'total', 'results'];
		var obj = {};
		att.forEach(function(k){
			if( typeof(x[k]) !== 'undefined' ){
				obj[k] = x[k];
			}
		});
		return obj;
		
	});
	
	var performanceTime = ( (new Date()) - start );
	log.debug('draft - time - check = %s (ms)', performanceTime);
	
	var table = {
		time: performanceTime,
		profile: profile,
		sports: topSportList,
		athletes: roster
	}
	
	return res.json(table);
};


