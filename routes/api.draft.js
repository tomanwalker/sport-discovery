
// ## dependencies
var express = require('express');
var server = express.Router();

var toolbox = require('../lib/toolbox');

// ### export
var ns = {router: server};
module.exports = ns;

// ### config
var log = toolbox.getLogger('routes/api.draft');

// ### func
ns.validateInput = function(req, res, next){
	
	log.debug('validate - start - user = %s | body = %j', res.locals.username, req.body);
	var draftSchema = toolbox.schemas.schema.draft;
	var err = toolbox.validate(req.body, draftSchema);
	
	if( err ){
		log.error('validate - failed - body = %j | err = %s', req.body, err.message);
		err.statusCode = 400;
		return next(err);
	}
	
	// special rules
	var rangePresent = (req.body.yearRangeStart && req.body.yearRangeEnd);
	var endBeforeStart = ( rangePresent && (req.body.yearRangeEnd - req.body.yearRangeStart) < 5 );
	if( endBeforeStart ){
		log.error('validate - error - body = %j | err = %s', req.body, 'End before Start');
		return next({statusCode: 400, message: '(yearEnd) has to be greater than (yearStart) by at least 5'});
	}
	
	return next();
	
};

ns.prepareQuery = function(req, res, next){
	log.debug('prepareQuery - permissions - user = %s | allow = %s | body = %j', 
		res.locals.username, res.locals.allow, req.body);
	var profile = req.body;
	var dataQuery = {
		sex: profile.gender, 
		"year_start": toolbox.params.YEAR_MIN,
		"year_end": toolbox.params.YEAR_MAX
	};
	
	profile.result_len = toolbox.params.RESULT_LEN_ELITE;
	
	if( req.body.checkSummer && !req.body.checkWinter ){
		profile.season = 'summer';
	}
	if( req.body.checkWinter && !req.body.checkSummer ){
		profile.season = 'winter';
	}
	if( profile.season ){
		dataQuery.season = profile.season;
	}
	profile.division = req.body.checkDivision;
	
	if( res.locals.user ){
		
		dataQuery.year_start = profile.yearRangeStart;
		dataQuery.year_end = profile.yearRangeEnd;
		profile.lg = req.body.sets;
		
		/*
		if( res.locals.user.allow > 100 ){
			profile.result_len = toolbox.params.RESULT_LEN_CASUAL;
			dataQuery.year_end = toolbox.params.YEAR_CASUAL;
		}
		if( res.locals.user.allow > 300 ){
		}
		*/
	}
	
	log.debug('prepareQuery - done - user = %s | query = %j', res.locals.username, dataQuery);
	res.locals.profile = profile;
	res.locals.dataQuery = dataQuery;
	return next();
	
};

ns.loadData = function(req, res, next){
	
	log.debug('loadData - start - user = %s | allow = %s | profile = %j', 
		res.locals.username, res.locals.allow, res.locals.profile);
	res.locals.timeStart = new Date();
	
	var data = null;
	
	if( res.locals.profile.lg ){
		log.debug('loadData - sets selected = %s', res.locals.profile.lg.join(',') );
		
		data = {
			cnt: 0,
			rows: []
		}
		
		res.locals.profile.lg.forEach(function(s){
			
			var loaded = toolbox.dal[s].search(res.locals.dataQuery);
			data.cnt += loaded.cnt;
			data.rows = data.rows.concat( loaded.rows );
			
		});
		
	}
	else {
		data = toolbox.dal.olympics.search(res.locals.dataQuery);
	}
	
	log.debug('loadData - got - cnt = %s', data.cnt);
	
	if( res.locals.profile.division ){
		res.locals.positions = require('./../data/reports/sub-presense.json');
		log.debug('loadData - division - pr_pc = %s | ts = %s', 
			res.locals.positions.pr_pc, res.locals.positions.ts);
		
		// TODO hook alert if older than 6 month
	}
	
	res.locals.data = data;
	return next();
};

ns.runDraft = function(req, res, next){
	
	var profile = res.locals.profile;
	var start = res.locals.timeStart;
	var dataList = res.locals.data.rows;
	log.debug('draft - start - user = %s | profile = %j', res.locals.username, profile);
	
	// groupping by sport, collect stats
	var grouppingResult = toolbox.analytics.group(dataList);
	
	var groups = grouppingResult.groups;
	var groupStats = grouppingResult.groupStats;
	var calcTable = grouppingResult.calcTable;
	
	log.debug('draft - groupping - amount = %s', Object.keys(groups).length);
	
	// evaluate height percentile & diff
	for(var k in groups){
		var low = 0.15;
		var mid = 0.50;
		var high = 0.85;
		
		var limit_step = 3;
		var limit_under = (mid*100) - limit_step; // => 47
		var limit_above = (mid*100) + limit_step;
		
		var len = calcTable[k].height.length;
		
		calcTable[k].height = calcTable[k].height.sort(); // ASC
		
		groupStats[k].h_min = calcTable[k].height[0];
		groupStats[k].h_max = calcTable[k].height[len-1];
		groupStats[k].h_low = Math.round( toolbox.analytics.pct(calcTable[k].height, low, true) );
		groupStats[k].h_mid = toolbox.analytics.round( toolbox.analytics.pct(calcTable[k].height, mid, true), 2);
		groupStats[k].h_high = Math.round( toolbox.analytics.pct(calcTable[k].height, high, true) );
		
		groupStats[k].h_avg = toolbox.analytics.round( (calcTable[k].h_sum / len), 1 );
		groupStats[k].h_avg_diff = toolbox.analytics.round(groupStats[k].h_avg - groupStats[k].h_mid, 1);
		groupStats[k].h_mid = Math.round(groupStats[k].h_mid);
		
		// profile match
		var checkHeight = toolbox.analytics.pctRank(calcTable[k].height, profile.height);
		groupStats[k].h_pct = 0.0;
		
		if( checkHeight > -1){
			groupStats[k].h_pct = toolbox.analytics.round(checkHeight * 100, 2);
			
			// sport Outliers influence profile.pctRank
			groupStats[k].h_pct_shift = groupStats[k].h_pct;
			if( groupStats[k].h_pct > 0 && groupStats[k].h_pct < 100 ){
				groupStats[k].h_pct_shift = toolbox.analytics.round((groupStats[k].h_pct - groupStats[k].h_avg_diff / 2), 2);
			}
		}
		//log.debug('DEBUG - k = %s | c = %s | p = %s', k, checkHeight, groupStats[k].h_pct);
		
		// a bit flexible around percentile
		groupStats[k].h_pct_diff = 0;
		if( groupStats[k].h_pct_shift >= limit_above ){
			groupStats[k].h_pct_diff = toolbox.analytics.round((groupStats[k].h_pct_shift - limit_above), 2);
		}
		if( groupStats[k].h_pct_shift <= limit_under ){
			groupStats[k].h_pct_diff = toolbox.analytics.round((limit_under - groupStats[k].h_pct_shift), 2);
		}
		
		// eval index
		groupStats[k].h_diff_pc = groupStats[k].h_pct_diff / limit_under;
		groupStats[k].index = toolbox.analytics.round((1 - groupStats[k].h_diff_pc) * 100, 1);
		if( groupStats[k].index < 0 ){
			groupStats[k].index = 0;
		}
		
	}
	
	log.debug('draft - group stats - sample = %j', groupStats["basketball"]);
	log.debug('draft - group stats - sample = %j', groupStats["football"]);
	log.debug('draft - group stats - sample = %j', groupStats["athletics"]);
	
	// look for matches
	var groupStatsList = Object.entries(groupStats).map( x => x[1] );
	
	var heightMatch = groupStatsList.filter(function(x){
		
		// perhaps here - exlude the ones MAX or MIN exceeded?
		return true;
		
	}).sort(function(a, b){
		
		var pct_diff = Math.round(a.h_pct_diff) - Math.round(b.h_pct_diff); // DESC
		if( pct_diff !== 0 ){
			return pct_diff;
		}
		
		var abs_diff = Math.abs(a.h_mid - profile.height) - Math.abs(b.h_mid - profile.height); // ASC
		return abs_diff;
		
	});
	log.debug('draft - calc - heightMatch = %j', heightMatch.slice(0, 5));
	
	var topSportList = heightMatch.map(function(x){
		var att = ['key', 'h_low', 'h_mid', 'h_high', 'index'];
		var obj = {};
		
		att.forEach(function(k){
			obj[k] = x[k];
		});
		
		return obj;
	}).slice(0, profile.result_len);
	
	var topKey = topSportList[0].key;
	var athleteSamples = [];
	
	topSportList.forEach(function(s){
		
		var samplePeople = groups[s.key].filter(function(x){
			return (
				Math.abs(x.height - profile.height) < 10
			);
		}).sort(function(a, b){
			
			// 1cm is not important
			var heightDiffA = Math.floor(Math.abs(a.height - profile.height) / 2); 
			var heightDiffB = Math.floor(Math.abs(b.height - profile.height) / 2);
			var heightDiffSort = heightDiffA - heightDiffB; // ASC
			
			if( heightDiffSort !== 0 ){
				return heightDiffSort;
			}
			
			var medalDiff = b.medal_number - a.medal_number; // DESC
			if( medalDiff !== 0 ){
				return medalDiff;
			}
			
			return b.year - a.year;  // DESC
		});
		
		if( samplePeople.length > 0 ){
			athleteSamples.push( samplePeople[0] );
		}
	});
	
	var roster = athleteSamples.map(function(x){
		
		var att = ['name', 'sport', 'country', 'height', 'weight', 'year', 
			'year_start', 'year_end', 'mvp', 'gold', 'silver', 'bronze'];
		var obj = {};
		att.forEach(function(k){
			if( typeof(x[k]) !== 'undefined' ){
				obj[k] = x[k];
			}
		});
		return obj;
		
	}).slice(0, profile.result_len);
	
	
	var h_dist = {};
	var h_per = {};
	var h_len = calcTable[topKey].height.length;
	calcTable[topKey].height.forEach(function(n){
		if( !h_dist[n] ){
			h_dist[n] = 0;
		}
		
		h_dist[n] += 1;
	});
	Object.keys(h_dist).forEach(function(x){
		h_per[x] = Math.round(h_dist[x] / h_len * 100);
	});
	
	log.debug('draft - calc - h_dist = %j | h_per = %j', h_dist, h_per);
	var performanceTime = ( (new Date()) - start );
	log.debug('draft - time - check = %s (ms)', performanceTime);
	
	if( !res.locals.user || (res.locals.user && res.locals.user.allow < 500) ){
		var userText = res.locals.username || 'anon';
		var msg = userText + "\n";
		msg += performanceTime + " (ms)";
		msg += JSON.stringify(profile);
		
		toolbox.hook.post('draft', msg);
		// TODO insert DB
	}
	
	// response - N sports, M athletes, 1 sport calcTable
	// anon - N = 3 / M = 3
	// login - N = 4 / M = 4
	// elite - N = 5 / M = 5
	var table = {
		time: performanceTime,
		sports: topSportList,
		athletes: roster
	};
	
	if( res.locals.user && res.locals.user.allow > 100 ){
		table.calc = {
			key: topKey,
			height: h_per
		};
	}
	
	return res.json(table);
};


