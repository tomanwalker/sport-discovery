
// ## dependencies
var express = require('express');
var server = express.Router();

var toolbox = require('../lib/toolbox');

// ### config
var log = toolbox.getLogger('routes/ui');
var webProtect = toolbox.auth.check_auth('/login');

// ### export
var ns = {router: server};
module.exports = ns;

// ### func
ns.adminCheck = function(req, res, next){
	log.debug('quality - perm - user = %s | allow = %s', res.locals.username, res.locals.allow);
	if( res.locals.user.allow < 500 ){
		log.error('quality - attempt to access not authorized content - user = %s', res.locals.username);
		res.locals.msg = 'Apologies. You are not authorized to view this page';
		var errPage = toolbox.render('error', res.locals);
		return res.send(errPage);
	}
	
	return next();
	
};

ns.serveQuality = async function(req, res){
	log.debug('quality - start - user = %s', res.locals.username);
	
	var data = await toolbox.dal.olympics.search({}, {quality: false});
	var markedDupl = toolbox.csv.readFile('./data/dupl.csv');
	
	var findDuplicates = function( arr ){
		
		var fil = arr.filter(function(x){
			return x.date_of_birth.length > 0;
		});
		fil = fil.sort(function(a,b){
			if (a.date_of_birth < b.date_of_birth){
				return -1;
			}
			if (a.date_of_birth > b.date_of_birth){
				return 1;
			}
			return 0;
		});
		var results = [];
		
		log.debug('look for duples - filtered = %s / %s', fil.length, arr.length);
		
		for(var i=0; i<fil.length-1; i++){
			
			var tenth = Math.round(fil.length / 36);
			var upto = Math.min(fil.length, i+tenth);
			
			for(var k=i+1; k<fil.length; k++){
				
				var foundMatch = (
					   fil[i].sport === fil[k].sport
					&& fil[i].sex === fil[k].sex
					&& fil[i].date_of_birth === fil[k].date_of_birth
					&& fil[i].year === fil[k].year
					&& i !== k
					&& fil[i].country === fil[k].country
					&& fil[i].name[0] === fil[k].name[0]
				);
				
				if ( foundMatch ) {
					results.push({one: fil[i], two: fil[k]});
				}
				
			}
		}
		
		results = results.filter(function(r){
			var alreadyKnown = false;
			
			markedDupl.forEach(function(d){
				var sp = d.names.split(',');
				
				if( sp.includes(r.one.name) && sp.includes(r.two.name) ){
					alreadyKnown = true;
				}
			});
			
			return !alreadyKnown;
		});
		
		results = results.map(x => x.one);
		
		return results;
	};
	
	var groupBySport = {};
	data.rows.forEach(function(x){
		if( !groupBySport[x.sport] ){
			groupBySport[x.sport] = 0;
		}
		
		groupBySport[x.sport] += 1;
	});
	
	var listLen = 5;
	var sorter = function(a,b){
		var medalDiff = (b.medal_number - a.medal_number); //DESC
		if( medalDiff !== 0 ){
			return medalDiff;
		}
		return Number(b.year) - Number(a.year); //DESC
	};
	
	res.locals.qa = {};
	res.locals.qa.sportList = groupBySport;
	
	var missHeight = data.rows.filter( x => x.height < 10 ).sort(sorter);
	res.locals.qa.missHeight = missHeight.slice(0, listLen);
	res.locals.qa.missHeightCnt = missHeight.length;
	
	var missWeight = data.rows.filter( x => x.weight < 10 ).sort(sorter);
	res.locals.qa.missWeight = missWeight.slice(0, listLen);
	res.locals.qa.missWeightCnt = missWeight.length;
	
	res.locals.qa.missBmi = data.rows.filter(function(x){ 
		return x.weight > 10 && x.height > 10 && x.bmi < 2;
	}).sort(sorter).slice(0, listLen);
	
	var missDob = data.rows.filter( x => x.date_of_birth === "" ).sort(sorter);
	res.locals.qa.missDob = missDob.slice(0, listLen);
	res.locals.qa.missDobCnt = missDob.length;
	
	var dupl = findDuplicates(data.rows).sort(sorter);
	res.locals.qa.dupl = dupl.slice(0, listLen);
	res.locals.qa.duplCnt = dupl.length;
	
	var missSub = data.rows.filter( x => x.sub === "" ).sort(sorter);
	res.locals.qa.missSub = missSub.slice(0, listLen);
	res.locals.qa.missSubCnt = missSub.length;
	
	var body = toolbox.render('quality', res.locals);
	return res.send(body);
};


