
// ## dependencies
var express = require('express');
var server = express.Router();

var toolbox = require('../lib/toolbox');

var apiDraft = require('./api.draft');
var apiDraft2 = require('./api.draft.v2');

// ### export
var ns = {router: server};
module.exports = ns;

// ### config
server.use(express.json()); // for parsing application/json
server.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

var log = toolbox.getLogger('routes/api');
var apiProtect = toolbox.auth.check_auth();

// ### func
/*
// v1
server.post('/api/draft',
	apiDraft.validateInput, apiDraft.prepareQuery, apiDraft.loadData, apiDraft.runDraft
);
*/
// v2
server.post('/api/draft',
	apiDraft.validateInput, apiDraft.prepareQuery, apiDraft.loadData, 
	apiDraft2.filterWeight, apiDraft2.groupData, apiDraft2.similarAthletes,
	apiDraft2.saveProfile, apiDraft2.prepareResponse
);

var testInput = function(req, res, next){
	
	req.body = {
		height: Number(req.query.h || 174),
		gender: 'male',
		weight: Number(req.query.w || 68),
		yearRangeStart: 1980,
		yearRangeEnd: 2018,
		checkSummer: true,
		sets: ['olympics', 'nba'],
		checkDivision: true
	}
	
	return next();
};
server.get('/api/draft-test', 
	testInput,
	apiDraft.validateInput, apiDraft.prepareQuery, apiDraft.loadData, 
	apiDraft2.filterWeight, apiDraft2.groupData, apiDraft2.similarAthletes,
	apiDraft2.saveProfile, apiDraft2.prepareResponse
);


server.post('/api/feedback', function(req, res, next){
	
	var feedbackSchema = {
		type: 'object',
		required: ['msg'],
		properties: {
			msg: {
				type: 'string',
				minLength: 5,
				maxLength: 500
			},
			contact: {
				type: 'boolean'
			}
		}
	};
	
	var err = toolbox.validate(req.body, feedbackSchema);
	if( err ){
		log.error('feedback.validation - failed - body = %j | err = %s', req.body, err.message);
		err.statusCode = 400;
		return next(err);
	}
	
	return next();
	
}, function(req, res, next){
	
	log.debug('feedback.post - start - user = %s | input = %j', res.locals.username, req.body);
	
	var date = new Date();
	var id = toolbox.dal.getTimeId();
	
	var obj = {
		_id: id,
		review: 0,
		user: null,
		timeCreate: date.toISOString(),
		timeReview: null
	};
	
	Object.assign(obj, req.body);
	
	if( res.locals.username ){
		obj.user = res.locals.username;
	}
	
	toolbox.dal.feedback.create(obj).then(function(result){
		log.debug('feedback.post - callback - user = %s | result = %j', res.locals.username, result);
		toolbox.hook.post('feedback', id);
		
		return res.json({message: 'ok'});
	}).catch(next);
	
});

server.post('/api/tm9j47i458xx8af4z7nmg1iu', function(req, res, next){
	// payment hook
	log.debug('payment.post1 - start - body = %j', req.body);
	toolbox.hook.post('paymentAPI', JSON.stringify(req.body) );
	
	// TODO some input validation would be nice anyway
	
	// event_type
	// "BILLING.SUBSCRIPTION.CREATED", "BILLING.SUBSCRIPTION.ACTIVATED"
	if( req.body.event_type === "BILLING.SUBSCRIPTION.ACTIVATED" ){
		toolbox.dal.payments.create( req.body );
	}
	
	return res.json({ ok: true });
	
});
server.post('/api/u5io3upa26bogdympqw3', function(req, res, next){
	// payment hook - IPN old page
	log.debug('payment.post2 - start - body = %j', req.body);
	toolbox.hook.post('paymentButton', JSON.stringify(req.body) );
	return res.json({ ok: true });
	
});

// TODO cleanup hooks [2020-11-03]

server.post('/api/hook/:kind/u5io3upa26bogdympqw3', async function(req, res, next){
	// payment hook - IPN old page
	log.debug('payment.post3 - start - kind = %s | content = %s | body = %j', 
		req.params.kind, req.get('Content-Type'), req.body);
	
	var realMap = {
		'ipn-sand': 'test',
		'test': 'test',
		'ipn': 'real'
	};
	
	if( req.body.custom && req.body.ipn_track_id ){
		/*
			"ipn_track_id": "e5b87df521f50"
			"payment_date": "23:49:34 Nov 02, 2020 PST"
			"payment_status": "Completed",
			"payer_email": "buyer@email.com",
			"custom": "userid=bb123",
			"payment_fee": "0.33",
			"payment_gross": "1.00",
			"mc_currency": "USD",
			"residence_country":"FI"
		*/
		
		var customList = req.body.custom.split(';');
		log.debug('payment.post3 - customList = %j', customList);
		var userid = customList.find( x => x.includes('userid') ).split('=')[1];
		
		var record = await toolbox.dal.users.get(userid);
		var current = new Date();
		var currentTime = current.toISOString();
		var payId = toolbox.dal.getTimeId();
		
		var pay_gross = Number(req.body.payment_gross);
		var pay_fee = Number(req.body.payment_fee);
		var pay_net = Math.round((pay_gross-pay_fee) * 100) / 100;
		var category = ( realMap[req.params.kind] || 'unknown' );
		
		var payObj = {
			_id: payId,
			kind: req.params.kind,
			category: category,
			userid: userid,
			username: record.username,
			timeCreate: currentTime,
			amount_gross: pay_gross,
			amount_fee: pay_fee,
			amount_net: pay_net,
			currency: req.body.mc_currency,
			country: req.body.residence_country,
			debug: req.body.custom,
			paypal_user: req.body.payer_email,
			payment_status: req.body.payment_status,
			payment_date: req.body.payment_date,
			track_id: req.body.ipn_track_id
		};
		
		await toolbox.dal.payments.create(payObj);
		
		if( record.allow < 500 ){
			record.allow = 310;
			record.timeUpdate = currentTime;
			record.rankExpieryMonth = 3;
			record.timeRank = currentTime;
			
			toolbox.dal.users.update(record); // dont wait for callback
		}
		
		// force User reload
		toolbox.dal.reload[userid] = current;
		
		toolbox.hook.post('paypal-ipn', JSON.stringify(payObj) );
		
	}
	else {
		toolbox.hook.post('paymentHook3', JSON.stringify(req.body) );
	}
	
	return res.json({ ok: true });
	
});

server.get('/api/me', apiProtect, function(req, res, next){
	log.debug('me - start - user = %s | allow = %s | fetch = %s | reload = %s', 
		res.locals.username, res.locals.user.allow, res.locals.user.fetch,
		toolbox.dal.reload[res.locals.user.id]);
	
	return res.json({ 
		id: res.locals.user.id,
		fetch: res.locals.user.fetch,
		allow: res.locals.user.allow
	});
});


