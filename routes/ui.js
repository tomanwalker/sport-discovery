
// ## dependencies
var express = require('express');
var server = express.Router();

var toolbox = require('../lib/toolbox');

var uiQuality = require('./ui.quality');

// ### config
var log = toolbox.getLogger('routes/ui');
var webProtect = toolbox.auth.check_auth('/login');

// ### export
var ns = {router: server};
module.exports = ns;

// ### func
server.get('/draft', function(req, res, next){
	log.debug('draft - start - user = %s | allow = %s | q = %j', 
		res.locals.username, res.locals.allow, req.query);
	if( req.session ){
		req.session.url = req.originalUrl;
	}
	/*
		{
		"user-agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0",
		"accept-language":"en-US,en;q=0.5",
		}
	*/
	/*
	Accept-Language: de
	Accept-Language: de-CH
	Accept-Language: en-US,en;q=0.5
	Accept-Language: fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5
	Accept-Language: ru-RU, ru;q=0.9, en-US;q=0.8, en;q=0.7, fr;q=0.6
	*/
	log.debug('draft - check - headers = %j', req.headers);
	
	var body = toolbox.render('draft', res.locals);
	return res.send(body);
});

server.get('/login', function(req, res){
	if( res.locals.user ){
		// already logged in
		return res.redirect('/');
	}
	log.debug('login - start - app_url = %s', toolbox.config.APP_URL);
	var obj = {r: (req.query.r || '')};
	var body = toolbox.render('login', obj);
	return res.send(body);
});

server.get('/stats', webProtect, async function(req, res, next){
	log.debug('stats - start - user = %s | allow = %s', res.locals.username, res.locals.allow);
	
	var metaInfo = await toolbox.dal.meta.get();
	// filter out other meta attributes
	res.locals.meta = {
		sets: metaInfo.sets
	};
	res.locals.agg = {cnt: 0, total: 0, h_min: 300, h_max: 0, y_min: 4000, y_max: 0};
	var rowsTable = 3;
	var rowsPie = 5;
	
	// include only "ready"
	res.locals.meta.sets = res.locals.meta.sets.map(function(s){
		
		// we need following States:
		// 0 - plan / wip / review - should not even read
		// 1 - test,ready/advanced/101 - should read Count only
		// 2 - test/advanced/303 - should read full
		// 3 - ready/advanced/303 - should read full, should display Ok, and Draft use
		
		s.ready = 0;
		if( s.state === 'test' ){
			s.ready = 1;
		}
		if( s.state === 'ready' ){
			s.ready = 2;
		}
		
		s.enabled = (
			s.ready > 0
			&& (
				(res.locals.user.allow < 300 && s.kind === 'core')
				|| res.locals.user.allow > 300
			)
		);
		
		return s;
	});
	log.debug('stats - permissions - allow = %s | sets = %j', res.locals.user.allow, res.locals.meta.sets);
	
	var readySets = res.locals.meta.sets.filter( x => x.ready > 0 );
	var sportTable = {};
	var heightTable = {};
	
	for( var s of readySets ){
		
		log.debug('stats - before - set = %s | enabled = %s', s.name, s.enabled);
		var data = await toolbox.dal[s.name].search();
		log.debug('stats - read - set = %s | cnt = %s | total = %s', s.name, data.cnt, data.total);
		
		s.cnt = data.cnt;
		s.total = data.total;
		
		if( !s.enabled ){
			continue;
		}
		
		res.locals.agg.total += data.total;
		res.locals.agg.cnt += data.cnt;
		
		data.rows.forEach(function(row){
			
			res.locals.agg.h_min = Math.min(res.locals.agg.h_min, row.height);
			res.locals.agg.h_max = Math.max(res.locals.agg.h_max, row.height);
			
			res.locals.agg.y_min = Math.min(res.locals.agg.y_min, (row.year_start || row.year) );
			res.locals.agg.y_max = Math.max(res.locals.agg.y_max, (row.year_end || row.year) );
			
			if( !sportTable[row.sport] ){
				sportTable[row.sport] = {
					label: row.sport, 
					cnt: 0,
					h_sum: 0,
					cnt_female: 0
				};
				heightTable[row.sport] = [];
			}
			sportTable[row.sport].cnt += 1;
			sportTable[row.sport].h_sum += row.height;
			
			if( row.sex === 'female' ){
				sportTable[row.sport].cnt_female += 1;
			}
			
			heightTable[row.sport].push( row.height );
			
		});
		
		// aggreate each sport
		var sportList = Object.entries(sportTable).map(function(tuple){
			var record = tuple[1];
			
			record.h_avg = Math.round(record.h_sum / record.cnt * 10) / 10;
			
			var sortedHeight = heightTable[tuple[0]].sort();
			record.h_low = Math.round(toolbox.analytics.pct(sortedHeight, 0.1, true));
			record.h_high = Math.round(toolbox.analytics.pct(sortedHeight, 0.9, true));
			record.h_range = record.h_high - record.h_low;
			
			record.female_pc = Math.round( record.cnt_female / record.cnt * 100 );
			
			return record;
		});
		
		//map only needed attributes
		var presenseList = sportList.map(function(x){
			return {
				label: x.label,
				cnt: x.cnt
			}
		});
		res.locals.agg.sportListPieMost = presenseList.sort( (a,b) => b.cnt - a.cnt ).slice(0, rowsPie); // DESC
		
		// super user ONLY
		if( res.locals.allow > 300 ){
			res.locals.agg.sportListPieLeast = presenseList.sort( (a,b) => a.cnt - b.cnt ).slice(0, rowsPie); // ASC
			
			var heightList = sportList.map(function(x){
				return {
					label: x.label,
					h_avg: x.h_avg
				}
			});
			res.locals.agg.sportListShort = heightList.sort( (a,b) => a.h_avg - b.h_avg ).slice(0, rowsTable); // ASC
			res.locals.agg.sportListTall = heightList.sort( (a,b) => b.h_avg - a.h_avg ).slice(0, rowsTable); // DESC
			
			var inclusiveList = sportList.map(function(x){
				return {
					label: x.label,
					h_range: x.h_range,
					h_low: x.h_low,
					h_high: x.h_high
				}
			});
			res.locals.agg.rangeLow = inclusiveList.sort( (a,b) => a.h_range - b.h_range ).slice(0, rowsTable); // ASC
			res.locals.agg.rangeHigh = inclusiveList.sort( (a,b) => b.h_range - a.h_range ).slice(0, rowsTable); // DESC
			
			var femalePresent = sportList.sort(function(a,b){
				var pcDiff = b.female_pc - a.female_pc; // DESC
				if( pcDiff !== 0 ){
					return pcDiff;
				}
				
				return a.cnt - b.cnt; // ASC
				
			}).map(function(x){
				return {
					label: x.label,
					pc: x.female_pc
				};
			});
			
			res.locals.agg.femalePieMost = femalePresent.slice(0, rowsPie);
		}
	}
	
	var body = toolbox.render('stats', res.locals);
	return res.send(body);
});

server.get('/quality', webProtect, uiQuality.adminCheck, uiQuality.serveQuality);

server.get('/feedback', function(req, res, next){
	log.debug('feedback.get - start - user = %s', res.locals.username);
	
	toolbox.dal.feedback.getLatest().then(function(result){
		
		log.debug('feedback.get - callback - result = %s', result.length);
		res.locals.feedback = result;
		
		var body = toolbox.render('feedback', res.locals);
		return res.send(body);
		
	}).catch(next);
});

server.get('/rank', webProtect, function(req, res, next){
	log.debug('rank - start - user = %s', res.locals.username);
	
	//res.locals.pp_client = toolbox.config.PAYPAL_CLIENT;
	var body = toolbox.render('rank', res.locals);
	return res.send(body);
	
});

server.get('/about', function(req, res, next){
	var body = toolbox.render('about', res.locals);
	return res.send(body);
});

server.get('/entry', function(req, res, next){
	var body = toolbox.render('front', res.locals);
	return res.send(body);
});

server.get('/articles', function(req, res, next){
	var body = toolbox.render('articles', res.locals);
	return res.send(body);
});

server.get('/articles/:id', function(req, res, next){
	
	var dict = {
		'height-in-sport-outliers': 'height-outliers',
		'nba-height-diversity': 'nba-diversity'
	};
	
	var id = req.params.id;
	var body = toolbox.render('articles/' + dict[id], res.locals);
	return res.send(body);
});

/*
var underwork = function(req, res, next){
	var body = toolbox.render('articles/nba-diversity', res.locals);
	return res.send(body);
};

server.get('/articles/new', underwork);
*/


