
// ## dependencies
var express = require('express');
var server = express.Router();

var toolbox = require('../lib/toolbox');

// ### export
module.exports = server;

// ### config
var log = toolbox.getLogger('router');
var webProtect = toolbox.auth.check_auth('/login');
var apiProtect = toolbox.auth.check_auth();

// ### flow

// static files
var filesPath = __dirname.replace('routes', 'public');
log.info('filesPath = %s', filesPath);
server.use('/', express.static(filesPath));

// Auth
toolbox.auth.init(server);

// TLS redirect
if( toolbox.config.CLOUD_RUN && toolbox.config.SECURE_REDIRECT === 'true' ){
	server.use('/', function(req, res, next){
		if(req.headers['x-forwarded-proto'] !== 'https') {
			return res.redirect(['https://', req.get('Host'), req.originalUrl].join(''));
		}
		
		return next();
	});
}

// base logging
server.use('/', function(req, res, next){
	
	var IGNORE = ['.png', '.css', '.js', '.jpg'];
	var skip = IGNORE.find( x => req.path.includes(x) );
	
	if( req.user && req.user.email ){
		res.locals.user = req.user;
		res.locals.username = req.user.email;
		res.locals.allow = req.user.allow;
	}
	
	
	var localeHeadString = req.get('Accept-Language') || 'none';
	var localeHeadList = localeHeadString.split(',');
	var localeHead = localeHeadList[0].trim();
	
	var localeUser = ( res.locals.user && res.locals.user.locale );
	res.locals.locale = (localeUser || localeHead);
	
	if( !skip ){
		log.debug('base - user = %s | locale = %s | path = %s %s | auth = %s', 
			res.locals.username, res.locals.locale, req.method, req.originalUrl, req.isAuthenticated() );
	}
	
	return next();
});

server.get('/', function(req, res, next){
	
	if( res.locals.username ){
		return res.redirect('/draft');
	}
	
	return res.redirect('/entry');
});

var uiModule = require('./ui').router;
server.use(uiModule);
var apiModule = require('./api').router;
server.use(apiModule);

// block list
var blockFile = __dirname.replace('routes', 'data') + '/block.csv';
var blockList = require('../lib/csv').readFile(blockFile);

server.use(function(req, res, next){
	
	var found = blockList.find( x => req.path.includes(x.url) );
	if( found ){
		return res.status(404).json({
			msg: 'not found'
		});
	}
	
	return next();
});

// 404 + Common err Handler
server.use(function(req, res, next){
	return next({
		status: 404,
		message: 'Not Found - ' + (req.method + ' ' + req.originalUrl)
	});
});

var ipKey = function(req){
	if( toolbox.config.CLOUD_RUN ){
		return req.headers['x-forwarded-for'].split(',')[0];
	}
	else{
		return req.ip;
	}
};
server.use(function(err, req, res, next){
	var statusToUse = (err.status || err.statusCode || 500);
	var msgToUse = (err.msg || err.message || err);
	
	log.error('common - error - user = %s | status = %s | url = %s %s | err = %s | stack = %s', 
		res.locals.username, statusToUse, req.method, req.originalUrl, msgToUse, err.stack);
	
	if( res.headerSent || msgToUse.includes('EPERM') ){
		return false;
	}
	
	var hookMessage = [
		'user = ', res.locals.username, ' | url = ', req.originalUrl,
		' | code = ', statusToUse, ' | msg = ', msgToUse,
		' | ip = ', ipKey(req)
	];
	if( req.body ){
		hookMessage.push(' | body = ');
		hookMessage.push(JSON.stringify(req.body));
	}
	toolbox.hook.post('error', hookMessage.join(''));
	
	if( !req.path.includes('api') && !req.path.includes('img') && statusToUse === 404){
		res.locals.msg = 'Apologies. Page does not exists';
		var body = toolbox.render('error', res.locals);
		return res.send(body);
	}
	return res.status(statusToUse).json({ message: msgToUse, details: err.details });
});


