
// ## dependencies
var express = require('express');
var server = express();
//var helmet = require('helmet');

var toolbox = require('./lib/toolbox');
var router = require('./routes/router');

// ## config
server.use(express.json());
//server.use(helmet());

var log = toolbox.getLogger('app.js');
toolbox.params.init();

// ## export
module.exports = server;

// ## flow
server.use(router);

log.info('server starting - port = %s', toolbox.config.PORT);
server.listen(toolbox.config.PORT, function(){
	log.info('server started...');
	toolbox.ping();
});




