

. secret.env

APP="sport-draft"


ibmcloud login --apikey $IBM_CLOUD_KEY -r eu-gb
ibmcloud target -o $ORG -s dev

ibmcloud cf push $APP -i 1 -m 500MB --no-start
# ibmcloud cf push $APP -n ${APP}-temp -i 1 -m 500MB --no-start

ibmcloud cf set-env $APP GOOGLE_ID $GOOGLE_ID
ibmcloud cf set-env $APP GOOGLE_SECRET $GOOGLE_SECRET
ibmcloud cf set-env $APP SESSION_SECRET $SESSION_SECRET

ibmcloud cf set-env $APP DB_HOST $DB_HOST
ibmcloud cf set-env $APP DB_KEY $DB_KEY
ibmcloud cf set-env $APP APP_URL "${PROTOCOL}://${DOMAIN}"
ibmcloud cf set-env $APP HOOK_EVENT $HOOK_EVENT

ibmcloud cf start $APP

ibmcloud cf map-route Green example.com -n demo-time

ibmcloud cf unmap-route Blue example.com -n demo-time

ibmcloud cf unmap-route Green example.com -n demo-time-temp

# ibmcloud cf stop $APP




