
// ## func
var calculateBMI = function(){
	
	var h = $('#height').val();
	var w = $('#weight').val();
	console.log('calculateBMI - start - h = %s | w = %s', h, w);
	
	if( h < 20 ){
		return false;
	}
	if( w < 2 ){
		return false;
	}
	
	var bmi = Math.round( w / Math.pow(h/100, 2) * 10) / 10;
	$('#bmi').val(bmi);
};

var calculateAgeGroup = function(){
	
	var dobText = $('#dob').val();
	console.log('calculateAgeGroup - start - dob = %s', dobText);
	var dob = new Date(dobText);
	var current = new Date();
	
	var years = current.getFullYear() - dob.getFullYear();
	console.log('calculateAgeGroup - diff - years = %s', years);
	$('#age').val('na');
	
	if( years >= 25 ){
		$('#age').val('adult 25+');
	}
	if( years >= 18 && years < 25 ){
		$('#age').val('adult 18+');
	}
	if( years >= 14 && years < 18 ){
		$('#age').val('child 14+');
	}
	if( years >= 6 && years < 12 ){
		$('#age').val('child 6+');
	}
	if( years >= 0 && years < 6 ){
		$('#age').val('child under 6');
	}
};

var calculateYearRange = function(){
	var yearStart = Number($("#yearRangeStart").val());
	var yearEnd = Number($("#yearRangeEnd").val());
	var yearDiff = yearEnd - yearStart;
	
	var yearDiffDisplay = Math.max(0, yearDiff);
	$("#yearRangeOut").val(yearDiffDisplay);
};

var collectInput = function(selector){
	var obj = {};
	
	$(selector).each(function(){
		
		var input = $(this);
		var id = input.attr('name') || input.attr('id');
		
		var val = input.val();
		var type = input.attr('type');
		var read = input.attr('readonly');
		var dis = input.attr('disabled');
		
		//console.log('DEBUG - type = %s | id = %s | val = %s | read = %s | dis = %s', 
		//	type, id, val, read, dis);
		
		if( id && val && !read && !dis ){
			
			if( type === 'checkbox' ){
				if( id.includes('inputlist') ){
					var listSp = id.split('-');
					var listName = listSp[1];
					var listVal = listSp[2];
					
					if( !obj[listName] ){
						obj[listName] = [];
					}
					if( input.is(':checked') ){
						obj[listName].push( listVal );
					}
				}
				else {
					obj[id] = input.is(':checked');
				}
			}
			else if( type === 'radio' ){
				obj[id] = $("input:radio[name ='"+id+"']:checked").val();
			}
			else if( type === 'number' ){
				obj[id] = Number(val);
			}
			else {
				obj[id] = val;
			}
			
		}
		
	});
	
	console.log('collectInput - collected - obj = %j', obj);
	return obj;
};

var validateInput = function(input, inputUnits){
	
	console.log('validateInput - start - input = %j', input);
	var schema = {
		required: ['gender', 'height'],
		properties: {
			gender: {
				type: 'string'
			},
			height: {
				type: 'integer',
				minimum: 140,
				maximum: 250
			}
		}
	};
	
	if( schema.required && schema.required.length ){
		for(var i=0; i<schema.required.length; i++){
			if( typeof(input[schema.required[i]]) === 'undefined' ){
				console.log('validateInput - required.fail - %s', schema.required[i]);
				return "missing required attribute - " + schema.required[i];
			}
		}
	}
	
	var attVal = null;
	if( schema.properties ){
		for(var p in schema.properties){
			if( typeof(input[p]) !== 'undefined' ){
				
				// type check
				
				if( typeof(schema.properties[p].minimum) !== 'undefined' ){
					if( input[p] < schema.properties[p].minimum ){
						attVal = schema.properties[p].minimum;
						if( inputUnits === 'imperical' ){
							attVal = convertHeightMetricText(schema.properties[p].minimum);
						}
						return (p + " - must have a minimum value of " + attVal);
					}
				}
				if( typeof(schema.properties[p].maximum) !== 'undefined' ){
					if( input[p] > schema.properties[p].maximum ){
						attVal = schema.properties[p].maximum;
						if( inputUnits === 'imperical' ){
							attVal = convertHeightMetricText(schema.properties[p].maximum);
						}
						return (p + " - must have a maximum value of " + attVal);
					}
				}
				
				
			}
		}
	}
	
	return null;
};

var elemSelector = "#msg";
var alertTimeObj = null;

var findPreviousAlertClass = function(selector){
	var classArr = $(selector)[0].className.split(' ');
	return classArr.find( x => x.includes("alert-") );
};
var resetAlertClass = function(selector){
	var classAlready = findPreviousAlertClass(selector);
	if( classAlready ){
		$(selector).removeClass( classAlready );
	}
};
var alertShow = function(msg, style){
	if( typeof(style) === 'undefined' ){
		style = "danger";
	}
	
	$(elemSelector).html(msg);
	
	resetAlertClass(elemSelector);
	$(elemSelector).addClass("alert-" + style);
};
var alertHideFunc = function(){
	$(elemSelector).html('');
	resetAlertClass(elemSelector);
};
var alertHide = function(time){
	
	if( time < 10 ){
		return alertHideFunc();
	}
	
	if( alertTimeObj ){
		clearTimeout(alertTimeObj);
		alertTimeObj = null;
	}
	alertTimeObj = setTimeout(alertHideFunc, time);
};
var showTitle = function(){
	var el = $(this);
	console.log('showTitle - start - id = %s / %s', el.prop("tagName").toLowerCase(), el.attr('id'));
	
	var displayText = el.attr('title-html') || el.attr('title');
	if( displayText ){
		alertShow(displayText, 'secondary');
		alertHide(5000);
	}
	return false;
};

var inputForUnits = function(){
	var selUnits = $("#units").val();
	
	if( selUnits === 'metric' ){
		$("#height-ig").hide();
		$("#height-ig :input").attr('disabled', true);
		$("#weight-ig").hide();
		$("#weight-ig :input").attr('disabled', true);
		
		$("#height-mg").show();
		$("#height-mg :input").attr('disabled', false);
		$("#weight-mg").show();
		$("#weight-mg :input").attr('disabled', false);
	}
	if( selUnits === 'imperical' ){
		$("#height-mg").hide();
		$("#height-mg :input").attr('disabled', true);
		$("#weight-mg").hide();
		$("#weight-mg :input").attr('disabled', true);
		
		$("#height-ig").show();
		$("#height-ig :input").attr('disabled', false);
		$("#weight-ig").show();
		$("#weight-ig :input").attr('disabled', false);
	}
};

var convertFeet = function(feet, inches){
	// feet and inches to CM
	return Math.round(Number(feet) * 30.48 + Number(inches) * 2.54);
};
var convertHeightMetric = function(cm){
	var feetDec = cm / 30.48;
	var feet = Math.floor(feetDec);
	var inch = Math.round((feetDec - feet) * 12);
	return [feet, inch];
};
var convertHeightMetricText = function(cm){
	var arr = convertHeightMetric(cm);
	return (arr[0] + "'" + arr[1] + '"');
};

var convertPound = function(kg){
	return Math.round(kg / 2.205);
};
var convertWeightMetric = function(kg){
	return Math.round(kg * 2.205);
};
var convertWeightMetricText = function(kg){
	var lb = convertWeightMetric(kg);
	return (lb + ' lb');
};



