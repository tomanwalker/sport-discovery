
// ## func
var createTableRowGenerator = function(cellType){
	return function(...cells){
		var row = "<tr>";
		cells.forEach(function(c){
			row += ("<"+cellType+">" + c + "</"+cellType+">");
		});
		row += "</tr>";
		return row;
	};
};

var createTableRow = createTableRowGenerator('td');
var createTableHeader = createTableRowGenerator('th');
var createTableMuted = createTableRowGenerator('td><div class="text-muted small" ');

function randomBetween(min, max) {
	// min and max included 
	return Math.floor(Math.random() * (max - min + 1) + min);
}

var doPost = function(url, body, retryMax, retryCount){
	
	if( typeof(retryCount) === 'undefined' ){
		retryCount = 1;
	}
	console.log(`doPost - start - body = ${JSON.stringify(body)} | cnt = ${retryCount}`);
	
	return new Promise(function(resolve, reject){
		var req = $.post({
			url: url,
			data: JSON.stringify(body),
			contentType: 'application/json'
		});
		req.done(function(data){
			return resolve(data);
		});
		
		req.fail(function(err) {
			console.log("doPost - fail - err = %j", err);
			
			if( err.status.toString()[0] === '5' && retryCount < retryMax ){
				return doPost(url, body, retryMax, retryCount+1);
			}
			
			return reject(err);
		});
		
		req.catch(function(err) {
			return reject(err);
		});
		
	});
	
};





